const zerorpc = require("zerorpc")
let client = new zerorpc.Client({ timeout: 3000, heartbeatInterval: 1000000 })

//{ timeout: 3000, heartbeatInterval: 300000 }
//timeout=600000,heartbeatInterval=600000

client.connect("tcp://127.0.0.1:4242")
//var fs = require('fs');
//text = fs.readFileSync('server','utf8').toString().split('\n')
//client.connect(text[0])

client.invoke("echo", "server ready", (error, res) => {
  if(error || res !== 'server ready') {
    console.error(error)
  } else {
    console.log("server is ready")
    insertarLetrasAnimadas(inTurnFadingTextG);
    document.getElementById("inTurnFadingTextG").style.display = "none"
  }
})


//----------------------------------------------------------------------------------------

let  neuronas = document.querySelector('#neuronas')
let alpha = document.querySelector('#alpha')
let activation = document.querySelector('#activation')
let simular = document.querySelector('#cbox_simular')
let bentrenar_net = document.querySelector('#bentrenar_net')
let ttaza_lograda = document.querySelector('#ttaza_lograda')
let  cbox_activar = document.querySelector('#cbox_activar')
let cbox_ajustar_extractor = document.querySelector('#cbox_ajustar_extractor')

cbox_activar.addEventListener('click', () => {
    if(cbox_activar.checked){
      alert("ADVERTENCIA!!: El sistema de clasificación fue probado con los parámetros por defecto.\
       Cualquier modificación a dichos parámetros puede ocasionar un comportamiento no esperado.");
       alpha.disabled=false
       neuronas.disabled=false
       activation.disabled=false
      }
     else {
       alpha.disabled=true
       neuronas.disabled=true
       activation.disabled=true
     }



})

//insertar letras animadas - Entrenando
function insertarLetrasAnimadas(inTurnFadingTextG) {

    var elem1 = document.createElement("div");
    var elem2 = document.createElement("div");
    var elem3 = document.createElement("div");
    var elem4 = document.createElement("div");
    var elem5 = document.createElement("div");
    var elem6 = document.createElement("div");
    var elem7 = document.createElement("div");
    var elem8 = document.createElement("div");
    var elem9 = document.createElement("div");
    var elem10 = document.createElement("div");
    //procesando
    elem1.setAttribute("id", "inTurnFadingTextG_1");
    elem1.setAttribute("class", "inTurnFadingTextG");
    elem1.innerHTML="E"

    elem2.setAttribute("id", "inTurnFadingTextG_2");
    elem2.setAttribute("class", "inTurnFadingTextG");
    elem2.innerHTML="n"

    elem3.setAttribute("id", "inTurnFadingTextG_3");
    elem3.setAttribute("class", "inTurnFadingTextG");
    elem3.innerHTML="t"

    elem4.setAttribute("id", "inTurnFadingTextG_4");
    elem4.setAttribute("class", "inTurnFadingTextG");
    elem4.innerHTML="r"

    elem5.setAttribute("id", "inTurnFadingTextG_5");
    elem5.setAttribute("class", "inTurnFadingTextG");
    elem5.innerHTML="e"

    elem6.setAttribute("id", "inTurnFadingTextG_6");
    elem6.setAttribute("class", "inTurnFadingTextG");
    elem6.innerHTML="n"

    elem7.setAttribute("id", "inTurnFadingTextG_7");
    elem7.setAttribute("class", "inTurnFadingTextG");
    elem7.innerHTML="a"

    elem8.setAttribute("id", "inTurnFadingTextG_8");
    elem8.setAttribute("class", "inTurnFadingTextG");
    elem8.innerHTML="n"

    elem9.setAttribute("id", "inTurnFadingTextG_9");
    elem9.setAttribute("class", "inTurnFadingTextG");
    elem9.innerHTML="d"

    elem10.setAttribute("id", "inTurnFadingTextG_10");
    elem10.setAttribute("class", "inTurnFadingTextG");
    elem10.innerHTML="o"


    inTurnFadingTextG.appendChild(elem1);
    inTurnFadingTextG.appendChild(elem2);
    inTurnFadingTextG.appendChild(elem3);
    inTurnFadingTextG.appendChild(elem4);
    inTurnFadingTextG.appendChild(elem5);
    inTurnFadingTextG.appendChild(elem6);
    inTurnFadingTextG.appendChild(elem7);
    inTurnFadingTextG.appendChild(elem8);
    inTurnFadingTextG.appendChild(elem9);
    inTurnFadingTextG.appendChild(elem10);
}

function is_entero_positivo(numero){
  var patron = /^\d*$/;
  if (patron.test(numero)) {
      return true;
    }else {
      return false;
  }
}


bentrenar_net.addEventListener('click', () => {
  if (neuronas.value!=null && neuronas.value!="" && neuronas.value>=5 && neuronas.value<=5000 && is_entero_positivo(neuronas.value)
      && alpha.value!=null && alpha.value!="" && alpha.value>=0 && alpha.value<=1 ){
      document.getElementById("inTurnFadingTextG").style.display = ""
      document.getElementById("circleloader").style.display = ""
      document.getElementById("checkmark").style.display = ""
      document.getElementById("circleloader").classList.remove('load-complete');
      bentrenar_net.disabled=true
      client.invoke("trainELMRenderer",neuronas.value,alpha.value,activation.value,simular.checked, (error, salida) => {
        if(error) {
          console.error(error)
        } else {
          ttaza_lograda.textContent = salida
          bentrenar_net.disabled=false
          document.getElementById("inTurnFadingTextG").style.display = "none"
          document.getElementById("circleloader").classList.add('load-complete');
          document.getElementById("checkmark").style.display = "block"

          //muestra la matriz de confusion lograda
          var elem = document.createElement("img");
          elem.setAttribute("src", 'tmp/confusion_matrix.png');
          elem.setAttribute("alt", "Flower");
          document.getElementById("imageMatrixConf").appendChild(elem);
          document.getElementById("imageMatrixConf").style.display = ""

        }
      })
    } else{
      alert("Por favor complete todos los campo requeridos y dentro del rango de valores permitidos.");
    }

})

cbox_ajustar_extractor.addEventListener('click', () => {

    if(cbox_ajustar_extractor.checked){
      document.getElementById("seleccionarCromosomasElm").style.display = ""
      //cbox_listo.checked=false
      document.getElementById("parametrosELM").style.display = "none"

      }
     else {
      document.getElementById("seleccionarCromosomasElm").style.display = "none"
     }
})
