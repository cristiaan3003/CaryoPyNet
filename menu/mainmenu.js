const {Menu} = require('electron')

const template = [
  {
    label: 'Configurar',
    submenu: [

      {
        label: 'Enderezar Cromosomas',
        accelerator: 'CmdOrCtrl+E',
        click: function () {
          let mainWindow = null
          const electron = require('electron')
          const BrowserWindow = electron.BrowserWindow
          const app2 = electron.app
          const path = require('path')


          const createWindow = () => {
            mainWindow = new BrowserWindow({width: 1024, height: 768, title: 'Straighten'})
            mainWindow.loadURL(require('url').format({
              pathname: path.join(__dirname, '../straighten.html'),
              protocol: 'file:',
              slashes: true
            }))
            //mainWindow.webContents.openDevTools()

            mainWindow.on('closed', () => {
              mainWindow = null
            })

          }
          createWindow()

         }
      },
      {
        label: 'Almacenar Cromosomas',
        accelerator: 'CmdOrCtrl+D',
        click: function () {
          let mainWindow = null
          const electron = require('electron')
          const BrowserWindow = electron.BrowserWindow
          const app2 = electron.app
          const path = require('path')


          const createWindow = () => {
            mainWindow = new BrowserWindow({width: 1024, height: 768, title: 'DumpImage'})
            mainWindow.loadURL(require('url').format({
              pathname: path.join(__dirname, '../dump.html'),
              protocol: 'file:',
              slashes: true
            }))
            //mainWindow.webContents.openDevTools()

            mainWindow.on('closed', () => {
              mainWindow = null
            })

          }
          createWindow()

         }
      },
      {
        label: 'Extraer Caracteristicas',
        accelerator: 'CmdOrCtrl+T',
        click: function () {
          let mainWindow = null
          const electron = require('electron')
          const BrowserWindow = electron.BrowserWindow
          const app2 = electron.app
          const path = require('path')


          const createWindow = () => {
            mainWindow = new BrowserWindow({width: 1024, height: 768, title: 'Train CNN'})
            mainWindow.loadURL(require('url').format({
              pathname: path.join(__dirname, '../net.html'),
              protocol: 'file:',
              slashes: true
            }))
            //mainWindow.webContents.openDevTools()

            mainWindow.on('closed', () => {
              mainWindow = null
            })

          }
          createWindow()

         }
      },
      {
        label: 'Entrenar ELM',
        accelerator: 'CmdOrCtrl+W',
        click: function () {
          let mainWindow = null
          const electron = require('electron')
          const BrowserWindow = electron.BrowserWindow
          const app2 = electron.app
          const path = require('path')


          const createWindow = () => {
            mainWindow = new BrowserWindow({width: 1024, height: 768, title: 'Train ELM'})
            mainWindow.loadURL(require('url').format({
              pathname: path.join(__dirname, '../elm.html'),
              protocol: 'file:',
              slashes: true
            }))
            //mainWindow.webContents.openDevTools()

            mainWindow.on('closed', () => {
              mainWindow = null
            })

          }
          createWindow()

         }
      },{
        label: 'Configuración Simple',
        accelerator: 'CmdOrCtrl+X',
        click: function () {
          let mainWindow = null
          const electron = require('electron')
          const BrowserWindow = electron.BrowserWindow
          const app2 = electron.app
          const path = require('path')


          const createWindow = () => {
            mainWindow = new BrowserWindow({width: 1024, height: 768, title: 'Configuración Simple'})
            mainWindow.loadURL(require('url').format({
              pathname: path.join(__dirname, '../simpleconfig.html'),
              protocol: 'file:',
              slashes: true
            }))
            //mainWindow.webContents.openDevTools()

            mainWindow.on('closed', () => {
              mainWindow = null
            })

          }
          createWindow()

         }
      },
    ]
  },/*
  {
    label: 'Editar',
    submenu: [
      {
        label: 'Deshacer',
        role: 'undo'
      },
      {
        label: 'Rehacer',
        role: 'redo'
      },
      {
        type: 'separator'
      },
      {
        label: 'Cortar',
        role: 'cut'
      },
      {
        label: 'Copiar',
        role: 'copy'
      },
      {
        label: 'Pegar',
        role: 'paste'
      },
      {
        label: 'Borrar',
        role: 'delete'
      },
      {
        label: 'Seleccionar todo',
        role: 'selectall'
      }
    ]
  },*/
  {
    label: 'Ver',
    submenu: [
      {
        label: 'Reset',
        accelerator: 'CmdOrCtrl+R',
        click (item, focusedWindow) {
          if (focusedWindow) focusedWindow.reload()
        }
      },
      {
        label: 'Alternar herramientas de desarrollo',
        accelerator: process.platform === 'darwin' ? 'Alt+Command+I' : 'Ctrl+Shift+I',
        click (item, focusedWindow) {
          if (focusedWindow) focusedWindow.webContents.toggleDevTools()
        }
      },
      {
        type: 'separator'
      },
      {
        label: 'Reset zoom',
        role: 'resetzoom'
      },
      {
        role: 'zoomin'
      },
      {
        role: 'zoomout'
      },
      {
        type: 'separator'
      },
      {
        label: 'Pantalla completa',
        role: 'togglefullscreen'
      }
    ]
  },
  {
    role: 'window',
    submenu: [
      {
        accelerator: 'CmdOrCtrl+M',
        label: 'Minimizar',
        role: 'minimize'
      },
      {
        accelerator: 'CmdOrCtrl+Q',
        label: 'Cerrar',
        role: 'close'
      }
    ]
  }/*,
  {
    label: 'Ayuda',
    role: 'help',
    submenu: [
      {
        label: 'Aprende más',
        click () { require('electron').shell.openExternal('http://electron.atom.io') }
      }
    ]
  }*/
]

/*if (process.platform === 'darwin') {
  const name = app.getName()
  template.unshift({
    label: name,
    submenu: [
      {
        role: 'about'
      },
      {
        type: 'separator'
      },
      {
        role: 'services',
        submenu: []
      },
      {
        type: 'separator'
      },
      {
        role: 'hide'
      },
      {
        role: 'hideothers'
      },
      {
        role: 'unhide'
      },
      {
        type: 'separator'
      },
      {
        role: 'quit'
      }
    ]
  })
  // Edit menu.
  template[1].submenu.push(
    {
      type: 'separator'
    },
    {
      label: 'Speech',
      submenu: [
        {
          role: 'startspeaking'
        },
        {
          role: 'stopspeaking'
        }
      ]
    }
  )
  // Window menu.
  template[3].submenu = [
    {
      label: 'Cerrar',
      accelerator: 'CmdOrCtrl+Q',
      role: 'close'
    },
    {
      label: 'Minimizar',
      accelerator: 'CmdOrCtrl+M',
      role: 'minimize'
    },
    {
      label: 'Zoom',
      role: 'zoom'
    },
    {
      type: 'separator'
    },
    {
      label: 'Traer todo al frente',
      role: 'front'
    }
  ]
}*/

const menu = Menu.buildFromTemplate(template)
Menu.setApplicationMenu(menu)
