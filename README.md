# CarioPyNet

######  Automatic kariogram builder

This tool takes the set of isolated chromosome images from a cell, performs the feature extraction and labeling and finally generates and shows the on-screen karyogram. A trained convolutional neural network is used for the characterization of the images, while an extreme learning machine is used for the classification stage. It requires select a folder with containing the 46 TIFF images (any file name), as in the provided example. The output of the system is a table with the chromosomes labelled; which is also downloadable as a PDF report.


# Demo version available

URL: http://sinc.unl.edu.ar/web-demo/cariopynet/


