const zerorpc = require("zerorpc")
let client = new zerorpc.Client({ timeout: 3000, heartbeatInterval: 100000000 })

//{ timeout: 3000, heartbeatInterval: 300000 }
//timeout=600000,heartbeatInterval=600000

client.connect("tcp://127.0.0.1:4242")
//var fs = require('fs'); // Load the File System to execute our common tasks (CRUD)
//text = fs.readFileSync('server','utf8').toString().split('\n')
//client.connect(text[0])

client.invoke("echo", "server ready", (error, res) => {
  if(error || res !== 'server ready') {
    console.error(error)
  } else {
    console.log("server is ready")
    insertarLetrasAnimadas(inTurnFadingTextG);
    document.getElementById("inTurnFadingTextG").style.display = "none"
  }
})


//----------------------------------------------------------------------------------------

const {dialog} = require('electron').remote;

//--------------------
//Seleccionar carpetas
let select_dir_cromosomas = document.querySelector('#select_dir_cromosomas')
let path_dir_cromosomas = document.querySelector('#path_dir_cromosomas')

select_dir_cromosomas.addEventListener('click',function(){
    let pathDirectory = dialog.showOpenDialog({properties: ['openDirectory']});
    if(pathDirectory === undefined){
        console.log("Directorio no seleccionado");
    }else{
          path_dir_cromosomas.value = pathDirectory;
          if (!!path_dir_salida.value){
            bEnderezar.disabled=false
          }
    }

},false);

let select_dir_salida = document.querySelector('#select_dir_salida')
let path_dir_salida = document.querySelector('#path_dir_salida')

select_dir_salida.addEventListener('click',function(){
    let pathDirectory = dialog.showOpenDialog({properties: ['openDirectory']});
    if(pathDirectory === undefined){
        console.log("Directorio no seleccionado");
    }else{
          path_dir_salida.value = pathDirectory;
          if (!!path_dir_cromosomas.value){
            bEnderezar.disabled=false
          }
    }

},false);


//insertar letras animadas
function insertarLetrasAnimadas(inTurnFadingTextG) {

    var elem1 = document.createElement("div");
    var elem2 = document.createElement("div");
    var elem3 = document.createElement("div");
    var elem4 = document.createElement("div");
    var elem5 = document.createElement("div");
    var elem6 = document.createElement("div");
    var elem7 = document.createElement("div");
    var elem8 = document.createElement("div");
    var elem9 = document.createElement("div");
    var elem10 = document.createElement("div");
    //procesando
    elem1.setAttribute("id", "inTurnFadingTextG_1");
    elem1.setAttribute("class", "inTurnFadingTextG");
    elem1.innerHTML="P"

    elem2.setAttribute("id", "inTurnFadingTextG_2");
    elem2.setAttribute("class", "inTurnFadingTextG");
    elem2.innerHTML="r"

    elem3.setAttribute("id", "inTurnFadingTextG_3");
    elem3.setAttribute("class", "inTurnFadingTextG");
    elem3.innerHTML="o"

    elem4.setAttribute("id", "inTurnFadingTextG_4");
    elem4.setAttribute("class", "inTurnFadingTextG");
    elem4.innerHTML="c"

    elem5.setAttribute("id", "inTurnFadingTextG_5");
    elem5.setAttribute("class", "inTurnFadingTextG");
    elem5.innerHTML="e"

    elem6.setAttribute("id", "inTurnFadingTextG_6");
    elem6.setAttribute("class", "inTurnFadingTextG");
    elem6.innerHTML="s"

    elem7.setAttribute("id", "inTurnFadingTextG_7");
    elem7.setAttribute("class", "inTurnFadingTextG");
    elem7.innerHTML="a"

    elem8.setAttribute("id", "inTurnFadingTextG_8");
    elem8.setAttribute("class", "inTurnFadingTextG");
    elem8.innerHTML="n"

    elem9.setAttribute("id", "inTurnFadingTextG_9");
    elem9.setAttribute("class", "inTurnFadingTextG");
    elem9.innerHTML="d"

    elem10.setAttribute("id", "inTurnFadingTextG_10");
    elem10.setAttribute("class", "inTurnFadingTextG");
    elem10.innerHTML="o"


    inTurnFadingTextG.appendChild(elem1);
    inTurnFadingTextG.appendChild(elem2);
    inTurnFadingTextG.appendChild(elem3);
    inTurnFadingTextG.appendChild(elem4);
    inTurnFadingTextG.appendChild(elem5);
    inTurnFadingTextG.appendChild(elem6);
    inTurnFadingTextG.appendChild(elem7);
    inTurnFadingTextG.appendChild(elem8);
    inTurnFadingTextG.appendChild(elem9);
    inTurnFadingTextG.appendChild(elem10);
}

//------------------------------------------------------------------
//Enderezar

let bEnderezar = document.querySelector('#bEnderezar')
let tEnderezar = document.querySelector('#tEnderezar')

bEnderezar.addEventListener('click', () => {

  document.getElementById("inTurnFadingTextG").style.display = ""
  document.getElementById("circleloader").style.display = ""
  document.getElementById("checkmark").style.display = ""
  document.getElementById("circleloader").classList.remove('load-complete');
  bEnderezar.disabled=true

  client.invoke("enderezarRenderer",path_dir_cromosomas.value,path_dir_salida.value,(error, salida) => {
    if(error) {
      console.error(error)
    } else {
      $('.circle-loader').toggleClass('load-complete');
      $('.checkmark').toggle();
      document.getElementById("inTurnFadingTextG").style.display = "none"
      tEnderezar.textContent = salida
    }
  })
})
