const zerorpc = require("zerorpc")
let client = new zerorpc.Client({ timeout: 3000, heartbeatInterval: 100000000 })


client.connect("tcp://127.0.0.1:4242")


client.invoke("echo", "server ready", (error, res) => {
  if(error || res !== 'server ready') {
    console.error(error)
  } else {
    console.log("server is ready")
    insertarLetrasAnimadas(inTurnFadingTextG);
    document.getElementById("inTurnFadingTextG").style.display = "none"
  }
})


//----------------------------------------------------------------------------------------

const {dialog} = require('electron').remote;

//--------------------
//Variables
let select_dir_cromosomas = document.querySelector('#select_dir_cromosomas')
let path_dir_cromosomas = document.querySelector('#path_dir_cromosomas')
let  neuronas = document.querySelector('#neuronas')
let alpha = document.querySelector('#alpha')
let activation = document.querySelector('#activation')
let ttaza_lograda = document.querySelector('#ttaza_lograda')
let  cbox_activar = document.querySelector('#cbox_activar')
let  learning_rate = document.querySelector('#learning_rate')
let  momentum = document.querySelector('#momentum')
let  numero_epocas = document.querySelector('#numero_epocas')



cbox_activar.addEventListener('click', () => {

    if(cbox_activar.checked){
      alert("ADVERTENCIA!!: El sistema fue probado con los parámetros por defecto.\
       Cualquier modificación a dichos parámetros puede ocasionar un comportamiento no esperado.");
        learning_rate.disabled=false
        momentum.disabled=false
        numero_epocas.disabled=false
        neuronas.disabled=false
        alpha.disabled=false
        activation.disabled=false
      }
     else {
       learning_rate.disabled=true
       momentum.disabled=true
       numero_epocas.disabled=true
       neuronas.disabled=true
       alpha.disabled=true
       activation.disabled=true
     }})

select_dir_cromosomas.addEventListener('click',function(){
    let pathDirectory = dialog.showOpenDialog({properties: ['openDirectory']});
    if(pathDirectory === undefined){
        console.log("Directorio no seleccionado");
    }else{
          path_dir_cromosomas.value = pathDirectory;
          bConfigurar.disabled=false

    }

},false);



//insertar letras animadas
function insertarLetrasAnimadas(inTurnFadingTextG) {

    var elem1 = document.createElement("div");
    var elem2 = document.createElement("div");
    var elem3 = document.createElement("div");
    var elem4 = document.createElement("div");
    var elem5 = document.createElement("div");
    var elem6 = document.createElement("div");
    var elem7 = document.createElement("div");
    var elem8 = document.createElement("div");
    var elem9 = document.createElement("div");
    var elem10 = document.createElement("div");
    var elem11 = document.createElement("div");
    var elem12 = document.createElement("div");
    //configurando
    elem1.setAttribute("id", "inTurnFadingTextG_1");
    elem1.setAttribute("class", "inTurnFadingTextG");
    elem1.innerHTML="C"

    elem2.setAttribute("id", "inTurnFadingTextG_2");
    elem2.setAttribute("class", "inTurnFadingTextG");
    elem2.innerHTML="o"

    elem3.setAttribute("id", "inTurnFadingTextG_3");
    elem3.setAttribute("class", "inTurnFadingTextG");
    elem3.innerHTML="n"

    elem4.setAttribute("id", "inTurnFadingTextG_4");
    elem4.setAttribute("class", "inTurnFadingTextG");
    elem4.innerHTML="f"

    elem5.setAttribute("id", "inTurnFadingTextG_5");
    elem5.setAttribute("class", "inTurnFadingTextG");
    elem5.innerHTML="i"

    elem6.setAttribute("id", "inTurnFadingTextG_6");
    elem6.setAttribute("class", "inTurnFadingTextG");
    elem6.innerHTML="g"

    elem7.setAttribute("id", "inTurnFadingTextG_7");
    elem7.setAttribute("class", "inTurnFadingTextG");
    elem7.innerHTML="u"

    elem8.setAttribute("id", "inTurnFadingTextG_8");
    elem8.setAttribute("class", "inTurnFadingTextG");
    elem8.innerHTML="r"

    elem9.setAttribute("id", "inTurnFadingTextG_9");
    elem9.setAttribute("class", "inTurnFadingTextG");
    elem9.innerHTML="a"

    elem10.setAttribute("id", "inTurnFadingTextG_10");
    elem10.setAttribute("class", "inTurnFadingTextG");
    elem10.innerHTML="n"

    elem11.setAttribute("id", "inTurnFadingTextG_10");
    elem11.setAttribute("class", "inTurnFadingTextG");
    elem11.innerHTML="d"

    elem12.setAttribute("id", "inTurnFadingTextG_10");
    elem12.setAttribute("class", "inTurnFadingTextG");
    elem12.innerHTML="o"


    inTurnFadingTextG.appendChild(elem1);
    inTurnFadingTextG.appendChild(elem2);
    inTurnFadingTextG.appendChild(elem3);
    inTurnFadingTextG.appendChild(elem4);
    inTurnFadingTextG.appendChild(elem5);
    inTurnFadingTextG.appendChild(elem6);
    inTurnFadingTextG.appendChild(elem7);
    inTurnFadingTextG.appendChild(elem8);
    inTurnFadingTextG.appendChild(elem9);
    inTurnFadingTextG.appendChild(elem10);
    inTurnFadingTextG.appendChild(elem11);
    inTurnFadingTextG.appendChild(elem12);
}

function is_entero_positivo(numero){
  var patron = /^\d*$/;
  if (patron.test(numero)) {
      return true;
    }else {
      return false;
  }
}

//------------------------------------------------------------------
//Enderezar

let bConfigurar = document.querySelector('#bConfigurar')
let tConfigurar = document.querySelector('#tConfigurar')

bConfigurar.addEventListener('click', () => {
  if (neuronas.value!=null && neuronas.value!="" && neuronas.value>=5 && neuronas.value<=5000 && is_entero_positivo(neuronas.value)
      && alpha.value!=null && alpha.value!="" && alpha.value>=0 && alpha.value<=1 &&
      learning_rate.value!=null && momentum.value!=null && numero_epocas.value!=null
        && learning_rate.value!="" && momentum.value!="" && numero_epocas.value!=""
        && learning_rate.value<=5 && learning_rate.value>=0.001 && momentum.value>=0
        && momentum.value<=1 && numero_epocas.value>=1 && numero_epocas.value<=100
        && is_entero_positivo(numero_epocas.value)){
        document.getElementById("inTurnFadingTextG").style.display = ""
        document.getElementById("circleloader").style.display = ""
        document.getElementById("checkmark").style.display = ""
        document.getElementById("circleloader").classList.remove('load-complete');
        bConfigurar.disabled=true


  client.invoke("configurarSimple",path_dir_cromosomas.value,learning_rate.value,momentum.value,numero_epocas.value,neuronas.value,alpha.value,activation.value,(error, salida) => {
    if(error) {
      console.error(error)
    } else {
      ttaza_lograda.textContent = salida
      $('.circle-loader').toggleClass('load-complete');
      $('.checkmark').toggle();
      document.getElementById("inTurnFadingTextG").style.display = "none"
      tConfigurar.textContent = salida
    }
  })
  } else{
    alert("Por favor complete todos los campo requeridos y dentro del rango de valores permitidos.");
  }
})
