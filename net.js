const zerorpc = require("zerorpc")
let client = new zerorpc.Client({ timeout: 3000, heartbeatInterval: 100000000 })

//{ timeout: 3000, heartbeatInterval: 300000 }
//timeout=600000,heartbeatInterval=600000

client.connect("tcp://127.0.0.1:4242")

//var fs = require('fs');
//text = fs.readFileSync('server','utf8').toString().split('\n')
//client.connect(text[0])

client.invoke("echo", "server ready", (error, res) => {
  if(error || res !== 'server ready') {
    console.error(error)
  } else {
    console.log("server is ready")
    insertarLetrasAnimadas(inTurnFadingTextG);
    document.getElementById("inTurnFadingTextG").style.display = "none"
    insertarLetrasAnimadas(inTurnFadingTextG2);
    document.getElementById("inTurnFadingTextG2").style.display = "none"

  }
})


//----------------------------------------------------------------------------------------

let  learning_rate = document.querySelector('#learning_rate')
let  momentum = document.querySelector('#momentum')
let  numero_epocas = document.querySelector('#numero_epocas')
let  simular = document.querySelector('#cbox_simular')

let  bentrenar_net = document.querySelector('#bentrenar_net')
let  ttaza_lograda = document.querySelector('#ttaza_lograda')
let  cbox_activar = document.querySelector('#cbox_activar')
let cbox_ajustar_extractor = document.querySelector('#cbox_ajustar_extractor')
let cbox_listo = document.querySelector('#cbox_listo')

cbox_activar.addEventListener('click', () => {

    if(cbox_activar.checked){
      alert("ADVERTENCIA!!: El sistema de clasificación fue probado con los parámetros por defecto.\
       Cualquier modificación a dichos parámetros puede ocasionar un comportamiento no esperado.");
        learning_rate.disabled=false
        momentum.disabled=false
        numero_epocas.disabled=false
      }
     else {
       learning_rate.disabled=true
       momentum.disabled=true
       numero_epocas.disabled=true
     }



})

cbox_listo.addEventListener('click', () => {

    if(cbox_listo.checked){
      document.getElementById("entrenarCNN").style.display = "none"
      document.getElementById("ExtraerCaracteristicas").style.display = ""
      cbox_ajustar_extractor.checked=false
      }
     else {
      document.getElementById("ExtraerCaracteristicas").style.display = "none"
     }
})

cbox_ajustar_extractor.addEventListener('click', () => {

    if(cbox_ajustar_extractor.checked){
      document.getElementById("entrenarCNN").style.display = ""
      document.getElementById("ExtraerCaracteristicas").style.display = "none"
      cbox_listo.checked=false
      }
     else {
      document.getElementById("entrenarCNN").style.display = "none"
     }
})



//insertar letras animadas - Entrenando
function insertarLetrasAnimadas(inTurnFadingTextG) {

    var elem1 = document.createElement("div");
    var elem2 = document.createElement("div");
    var elem3 = document.createElement("div");
    var elem4 = document.createElement("div");
    var elem5 = document.createElement("div");
    var elem6 = document.createElement("div");
    var elem7 = document.createElement("div");
    var elem8 = document.createElement("div");
    var elem9 = document.createElement("div");
    var elem10 = document.createElement("div");
    //procesando
    elem1.setAttribute("id", "inTurnFadingTextG_1");
    elem1.setAttribute("class", "inTurnFadingTextG");
    elem1.innerHTML="E"

    elem2.setAttribute("id", "inTurnFadingTextG_2");
    elem2.setAttribute("class", "inTurnFadingTextG");
    elem2.innerHTML="n"

    elem3.setAttribute("id", "inTurnFadingTextG_3");
    elem3.setAttribute("class", "inTurnFadingTextG");
    elem3.innerHTML="t"

    elem4.setAttribute("id", "inTurnFadingTextG_4");
    elem4.setAttribute("class", "inTurnFadingTextG");
    elem4.innerHTML="r"

    elem5.setAttribute("id", "inTurnFadingTextG_5");
    elem5.setAttribute("class", "inTurnFadingTextG");
    elem5.innerHTML="e"

    elem6.setAttribute("id", "inTurnFadingTextG_6");
    elem6.setAttribute("class", "inTurnFadingTextG");
    elem6.innerHTML="n"

    elem7.setAttribute("id", "inTurnFadingTextG_7");
    elem7.setAttribute("class", "inTurnFadingTextG");
    elem7.innerHTML="a"

    elem8.setAttribute("id", "inTurnFadingTextG_8");
    elem8.setAttribute("class", "inTurnFadingTextG");
    elem8.innerHTML="n"

    elem9.setAttribute("id", "inTurnFadingTextG_9");
    elem9.setAttribute("class", "inTurnFadingTextG");
    elem9.innerHTML="d"

    elem10.setAttribute("id", "inTurnFadingTextG_10");
    elem10.setAttribute("class", "inTurnFadingTextG");
    elem10.innerHTML="o"


    inTurnFadingTextG.appendChild(elem1);
    inTurnFadingTextG.appendChild(elem2);
    inTurnFadingTextG.appendChild(elem3);
    inTurnFadingTextG.appendChild(elem4);
    inTurnFadingTextG.appendChild(elem5);
    inTurnFadingTextG.appendChild(elem6);
    inTurnFadingTextG.appendChild(elem7);
    inTurnFadingTextG.appendChild(elem8);
    inTurnFadingTextG.appendChild(elem9);
    inTurnFadingTextG.appendChild(elem10);
}


function is_entero_positivo(numero){
  var patron = /^\d*$/;
  if (patron.test(numero)) {
      return true;
    }else {
      return false;
  }
}


bentrenar_net.addEventListener('click', () => {

  if (learning_rate.value!=null && momentum.value!=null && numero_epocas.value!=null
    && learning_rate.value!="" && momentum.value!="" && numero_epocas.value!=""
    && learning_rate.value<=5 && learning_rate.value>=0.001 && momentum.value>=0
    && momentum.value<=1 && numero_epocas.value>=1 && numero_epocas.value<=100
    && is_entero_positivo(numero_epocas.value)){

    document.getElementById("inTurnFadingTextG").style.display = ""
    document.getElementById("circleloader").style.display = ""
    document.getElementById("checkmark").style.display = ""
    document.getElementById("circleloader").classList.remove('load-complete');
    bentrenar_net.disabled=true
    bextraer.disabled=true

      client.invoke("trainNetRenderer",learning_rate.value,momentum.value,numero_epocas.value, simular.checked, (error, salida) => {
        if(error) {
          console.error(error)
        } else {
          ttaza_lograda.textContent = salida
          bentrenar_net.disabled=false
          bextraer.disabled=false
          document.getElementById("inTurnFadingTextG").style.display = "none"
          document.getElementById("circleloader").classList.add('load-complete');
          document.getElementById("checkmark").style.display = "block"

          //muestra la matriz de confusion lograda
          var elem = document.createElement("img");
          elem.setAttribute("src", 'tmp/confusion_matrix_cnn.png');
          elem.setAttribute("alt", "Flower");
          document.getElementById("imageMatrixConf").appendChild(elem);
          document.getElementById("imageMatrixConf").style.display = ""

        }
      })
    }
  else {
    alert("Por favor complete todos los campo requeridos y dentro del rango de valores permitidos.");
  }


})


//insertar letras animadas
function insertarLetrasAnimadas(inTurnFadingTextG2) {

    var elem1 = document.createElement("div");
    var elem2 = document.createElement("div");
    var elem3 = document.createElement("div");
    var elem4 = document.createElement("div");
    var elem5 = document.createElement("div");
    var elem6 = document.createElement("div");
    var elem7 = document.createElement("div");
    var elem8 = document.createElement("div");
    var elem9 = document.createElement("div");
    var elem10 = document.createElement("div");
    //procesando
    elem1.setAttribute("id", "inTurnFadingTextG_1");
    elem1.setAttribute("class", "inTurnFadingTextG");
    elem1.innerHTML="P"

    elem2.setAttribute("id", "inTurnFadingTextG_2");
    elem2.setAttribute("class", "inTurnFadingTextG");
    elem2.innerHTML="r"

    elem3.setAttribute("id", "inTurnFadingTextG_3");
    elem3.setAttribute("class", "inTurnFadingTextG");
    elem3.innerHTML="o"

    elem4.setAttribute("id", "inTurnFadingTextG_4");
    elem4.setAttribute("class", "inTurnFadingTextG");
    elem4.innerHTML="c"

    elem5.setAttribute("id", "inTurnFadingTextG_5");
    elem5.setAttribute("class", "inTurnFadingTextG");
    elem5.innerHTML="e"

    elem6.setAttribute("id", "inTurnFadingTextG_6");
    elem6.setAttribute("class", "inTurnFadingTextG");
    elem6.innerHTML="s"

    elem7.setAttribute("id", "inTurnFadingTextG_7");
    elem7.setAttribute("class", "inTurnFadingTextG");
    elem7.innerHTML="a"

    elem8.setAttribute("id", "inTurnFadingTextG_8");
    elem8.setAttribute("class", "inTurnFadingTextG");
    elem8.innerHTML="n"

    elem9.setAttribute("id", "inTurnFadingTextG_9");
    elem9.setAttribute("class", "inTurnFadingTextG");
    elem9.innerHTML="d"

    elem10.setAttribute("id", "inTurnFadingTextG_10");
    elem10.setAttribute("class", "inTurnFadingTextG");
    elem10.innerHTML="o"


    inTurnFadingTextG2.appendChild(elem1);
    inTurnFadingTextG2.appendChild(elem2);
    inTurnFadingTextG2.appendChild(elem3);
    inTurnFadingTextG2.appendChild(elem4);
    inTurnFadingTextG2.appendChild(elem5);
    inTurnFadingTextG2.appendChild(elem6);
    inTurnFadingTextG2.appendChild(elem7);
    inTurnFadingTextG2.appendChild(elem8);
    inTurnFadingTextG2.appendChild(elem9);
    inTurnFadingTextG2.appendChild(elem10);
}

//----------------------------------------------------------------------------------------
//Extraer Caracteristicas
let bextraer = document.querySelector('#bextraer')
let finalizo = document.querySelector('#finalizo')

bextraer.addEventListener('click', () => {
  const path = require('path')
  const PY_NET_FOLDER = "data/net/net1.plk"
  const fullPath = path.join(__dirname, PY_NET_FOLDER)
  if (require('fs').existsSync(fullPath)){ //si la red ya fue entrenada el archivo debe existir
    document.getElementById("inTurnFadingTextG2").style.display = ""
    document.getElementById("circleloader2").style.display = ""
    document.getElementById("checkmark2").style.display = ""
    document.getElementById("circleloader2").classList.remove('load-complete');
    bextraer.disabled=true
    bentrenar_net.disabled=true
    client.invoke("extraerRenderer", (error, salida) => {
      if(error) {
        console.error(error)
      } else {

        bextraer.disabled=false
        bentrenar_net.disabled=false
        document.getElementById("inTurnFadingTextG2").style.display = "none"
        document.getElementById("circleloader2").classList.add('load-complete');
        document.getElementById("checkmark2").style.display = "block"
        finalizo.textContent = salida

      }
    })

  } else{ // si no existe el archivo por que no due entrenada se envia un aviso
    alert('Ajuste el extractor de características antes de extraerlas');
  }

})
