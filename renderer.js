const zerorpc = require("zerorpc")
let client = new zerorpc.Client({ timeout: 3000, heartbeatInterval: 1000000 })

//{ timeout: 3000, heartbeatInterval: 300000 }
//timeout=600000,heartbeatInterval=600000

client.connect("tcp://127.0.0.1:4242")
//var fs = require('fs'); // Load the File System to execute our common tasks (CRUD)
//text = fs.readFileSync('server','utf8').toString().split('\n')
//client.connect(text[0])

client.invoke("echo", "server ready", (error, res) => {
  if(error || res !== 'server ready') {
    console.error(error)
  } else {
    console.log("server is ready")
    insertarLetrasAnimadas(inTurnFadingTextG);
    document.getElementById("inTurnFadingTextG").style.display = "none"
    var fs = require('fs'); // Load the File System to execute our common tasks (CRUD)
    fs.readFile('./data/dump/corpus_entrenado.txt', 'utf-8', (err, data) => {
        if(err){
            //alert("An error ocurred reading the file :" + err.message);
            tresultado_img.textContent = "Modelo no entrenado.";
            return;
        }
        tresultado_img.textContent = "Modelo entrenado usando: "+data;
        // Change how to handle the file content
        //console.log("The file content is : " + data);
    });


    client.invoke("getDev", (error, salida) => {
        if(error) {
          console.error(error)
        } else {
          //codigo
          document.getElementById("deviceSpanInfo").setAttribute("data-balloon",salida);

        }
      })


  }
})


//----------------------------------------------------------------------------------------

const {dialog} = require('electron').remote;

//--------------------
//seleccionar IMAGEN
let select_dir = document.querySelector('#select_dir')
let path_dir_img = document.querySelector('#path_dir_img')
let bclasificar_img = document.querySelector('#bclasificar_img')
let bexport_pdf = document.querySelector('#cmd')
let tresultado_img = document.querySelector('#tresultado_img')
let contenido = document.querySelector('#content');

select_dir.addEventListener('click',function(){
    let pathDirectory = dialog.showOpenDialog({properties: ['openDirectory']});
    if(pathDirectory === undefined){
        console.log("Directorio no seleccionado");
    }else{
          bclasificar_img.disabled=false
          path_dir_img.value = pathDirectory;


    }

},false);

//------------------------------------------------------------------
//resultado_clasifica

//inserta una imagen y su url(urll) dentro de un "li"
function insertarImagen(list, urll) {
    var elem = document.createElement("img");
    var entry = document.createElement('li');
    elem.setAttribute("src", urll);
    elem.setAttribute("alt", "Flower");
    entry.style.listStyle="none"
    entry.style.cssFloat="left"
    entry.appendChild(elem);
    list.appendChild(entry);
}


//insertar letras animadas - clasificando
function insertarLetrasAnimadas(inTurnFadingTextG) {

    var elem1 = document.createElement("div");
    var elem2 = document.createElement("div");
    var elem3 = document.createElement("div");
    var elem4 = document.createElement("div");
    var elem5 = document.createElement("div");
    var elem6 = document.createElement("div");
    var elem7 = document.createElement("div");
    var elem8 = document.createElement("div");
    var elem9 = document.createElement("div");
    var elem10 = document.createElement("div");
    var elem11 = document.createElement("div");
    var elem12 = document.createElement("div");
    //procesando
    elem1.setAttribute("id", "inTurnFadingTextG_1");
    elem1.setAttribute("class", "inTurnFadingTextG");
    elem1.innerHTML="C"

    elem2.setAttribute("id", "inTurnFadingTextG_2");
    elem2.setAttribute("class", "inTurnFadingTextG");
    elem2.innerHTML="l"

    elem3.setAttribute("id", "inTurnFadingTextG_3");
    elem3.setAttribute("class", "inTurnFadingTextG");
    elem3.innerHTML="a"

    elem4.setAttribute("id", "inTurnFadingTextG_4");
    elem4.setAttribute("class", "inTurnFadingTextG");
    elem4.innerHTML="s"

    elem5.setAttribute("id", "inTurnFadingTextG_5");
    elem5.setAttribute("class", "inTurnFadingTextG");
    elem5.innerHTML="i"

    elem6.setAttribute("id", "inTurnFadingTextG_6");
    elem6.setAttribute("class", "inTurnFadingTextG");
    elem6.innerHTML="f"

    elem7.setAttribute("id", "inTurnFadingTextG_7");
    elem7.setAttribute("class", "inTurnFadingTextG");
    elem7.innerHTML="i"

    elem8.setAttribute("id", "inTurnFadingTextG_8");
    elem8.setAttribute("class", "inTurnFadingTextG");
    elem8.innerHTML="c"

    elem9.setAttribute("id", "inTurnFadingTextG_9");
    elem9.setAttribute("class", "inTurnFadingTextG");
    elem9.innerHTML="a"

    elem10.setAttribute("id", "inTurnFadingTextG_10");
    elem10.setAttribute("class", "inTurnFadingTextG");
    elem10.innerHTML="n"

    elem11.setAttribute("id", "inTurnFadingTextG_11");
    elem11.setAttribute("class", "inTurnFadingTextG");
    elem11.innerHTML="d"

    elem12.setAttribute("id", "inTurnFadingTextG_12");
    elem12.setAttribute("class", "inTurnFadingTextG");
    elem12.innerHTML="o"


    inTurnFadingTextG.appendChild(elem1);
    inTurnFadingTextG.appendChild(elem2);
    inTurnFadingTextG.appendChild(elem3);
    inTurnFadingTextG.appendChild(elem4);
    inTurnFadingTextG.appendChild(elem5);
    inTurnFadingTextG.appendChild(elem6);
    inTurnFadingTextG.appendChild(elem7);
    inTurnFadingTextG.appendChild(elem8);
    inTurnFadingTextG.appendChild(elem9);
    inTurnFadingTextG.appendChild(elem10);
    inTurnFadingTextG.appendChild(elem11);
    inTurnFadingTextG.appendChild(elem12);

}

//insertar letras animadas - clasificando
function setArchivoTexto(path,elem){
  var fs = require('fs'); // Load the File System to execute our common tasks (CRUD)
  fs.readFile(path, 'utf-8', (err, data) => {
      if(err){
          //alert("An error ocurred reading the file :" + err.message);
          elem.innerHTML = "";
          return;
      }

      elem.innerHTML = data;
      //elem.style.display = ""
      // Change how to handle the file content
      //console.log("The file content is : " + data);
  });
}




//Generar Cariograma ----------------------------------------------------
bclasificar_img.addEventListener('click', () => {

        document.getElementById("inTurnFadingTextG").style.display = ""
        document.getElementById("circleloader").style.display = ""
        document.getElementById("checkmark").style.display = ""
        document.getElementById("circleloader").classList.remove('load-complete');
        bclasificar_img.disabled=true
        //Inicia Info al usuario: Iniciando ...
             // luego es actualizada la leyenda con
             // document.addEventListener --> setInterval(myTimer ,1000); clearInterval(myVar)
        /*setInterval(function(){
               var elem = document.getElementById("info_generando_cario");
               setArchivoTexto('./tmp/info_generando_cario',elem);
               elem.style.display = "";
           }, 100);*/
        //llamo al metodo Python
        client.invoke("recuperaClaseRenderer",path_dir_img.value, (error, salida) => {
          if(error) {
            console.error(error)
          } else {
              //tresultado_img.textContent = salida
              if (salida==0){
              //  alert("falta");
              alert('Falta configurar parte del modelo. Si es el uso inicial del sistema se sugiere realizar la configuración inicial desde la barra de menú - Configuración');
              document.getElementById("inTurnFadingTextG").style.display = "none"
              document.getElementById("circleloader").style.display = "none"
              document.getElementById("checkmark").style.display = "none"

            }else{
              $('.circle-loader').toggleClass('load-complete');
              $('.checkmark').toggle();
              document.getElementById("inTurnFadingTextG").style.display = "none"
              document.getElementById("info_generando_cario").style.display = "none"
              //bexport_pdf.disabled=false
              document.getElementById("content").style.display = ""
              document.getElementById("cmd").style.display = ""
              document.getElementById("fecha").innerHTML = "Fecha:"+Date();
              for (i = 0; i < salida.length; i++) {
                i_esimo=salida[i]
                i_clase="clase"+i_esimo[0]
                var list = document.getElementById(i_clase);
                insertarImagen(list, i_esimo[1])
              }

              if( $('#clase1 li').size()>2){
                //alert('mas de dos');
                document.getElementById("clase1").style.backgroundColor = "green";
              };
              if( $('#clase2 li').size()>2){
                //alert('mas de dos');
                document.getElementById("clase2").style.backgroundColor = "green";
              };
              if( $('#clase3 li').size()>2){
                //alert('mas de dos');
                document.getElementById("clase3").style.backgroundColor = "green";
              };
              if( $('#clase4 li').size()>2){
                //alert('mas de dos');
                document.getElementById("clase4").style.backgroundColor = "green";
              };
              if( $('#clase5 li').size()>2){
                //alert('mas de dos');
                document.getElementById("clase5").style.backgroundColor = "green";
              };
              if( $('#clase6 li').size()>2){
                //alert('mas de dos');
                document.getElementById("clase6").style.backgroundColor = "green";
              };
              if( $('#clase7 li').size()>2){
                //alert('mas de dos');
                document.getElementById("clase7").style.backgroundColor = "green";
              };
              if( $('#clase8 li').size()>2){
                //alert('mas de dos');
                document.getElementById("clase8").style.backgroundColor = "green";
              };
              if( $('#clase9 li').size()>2){
                //alert('mas de dos');
                document.getElementById("clase9").style.backgroundColor = "green";
              };
              if( $('#clase10 li').size()>2){
                //alert('mas de dos');
                document.getElementById("clase10").style.backgroundColor = "green";
              };
              if( $('#clase11 li').size()>2){
                //alert('mas de dos');
                document.getElementById("clase11").style.backgroundColor = "green";
              };
              if( $('#clase12 li').size()>2){
                //alert('mas de dos');
                document.getElementById("clase12").style.backgroundColor = "green";
              };
              if( $('#clase13 li').size()>2){
                //alert('mas de dos');
                document.getElementById("clase13").style.backgroundColor = "green";
              };
              if( $('#clase14 li').size()>2){
                //alert('mas de dos');
                document.getElementById("clase14").style.backgroundColor = "green";
              };
              if( $('#clase15 li').size()>2){
                //alert('mas de dos');
                document.getElementById("clase15").style.backgroundColor = "green";
              };
              if( $('#clase16 li').size()>2){
                //alert('mas de dos');
                document.getElementById("clase16").style.backgroundColor = "green";
              };
              if( $('#clase17 li').size()>2){
                //alert('mas de dos');
                document.getElementById("clase17").style.backgroundColor = "green";
              };
              if( $('#clase18 li').size()>2){
                //alert('mas de dos');
                document.getElementById("clase18").style.backgroundColor = "green";
              };
              if( $('#clase19 li').size()>2){
                //alert('mas de dos');
                document.getElementById("clase19").style.backgroundColor = "green";
              };
              if( $('#clase20 li').size()>2){
                //alert('mas de dos');
                document.getElementById("clase20").style.backgroundColor = "green";
              };
              if( $('#clase21 li').size()>2){
                //alert('mas de dos');
                document.getElementById("clase21").style.backgroundColor = "green";
              };
              if( $('#clase22 li').size()>2){
                //alert('mas de dos');
                document.getElementById("clase22").style.backgroundColor = "green";
              };
              if( $('#clase23 li').size()>2){
                //alert('mas de dos');
                document.getElementById("clase23").style.backgroundColor = "green";
              };
              if( $('#clase24 li').size()>2){
                //alert('mas de dos');
                document.getElementById("clase24").style.backgroundColor = "green";
              };

            }


        }//else
        })

});


document.addEventListener("mousemove", myFunction);

function myFunction() {
  //Test si cambio la cantidad
  if( $('#clase1 li').size()>2){
    //alert('mas de dos');
    document.getElementById("clase1").style.backgroundColor = "green";
  }else{
    document.getElementById("clase1").style.backgroundColor = "white";
  }
  //Test si cambio la cantidad
  if( $('#clase2 li').size()>2){
    //alert('mas de dos');
    document.getElementById("clase2").style.backgroundColor = "green";
  }else{
    document.getElementById("clase2").style.backgroundColor = "white";
  }
  //Test si cambio la cantidad
  if( $('#clase3 li').size()>2){
    //alert('mas de dos');
    document.getElementById("clase3").style.backgroundColor = "green";
  }else{
    document.getElementById("clase3").style.backgroundColor = "white";
  }
  //Test si cambio la cantidad
  if( $('#clase4 li').size()>2){
    //alert('mas de dos');
    document.getElementById("clase4").style.backgroundColor = "green";
  }else{
    document.getElementById("clase4").style.backgroundColor = "white";
  }
  //Test si cambio la cantidad
  if( $('#clase5 li').size()>2){
    //alert('mas de dos');
    document.getElementById("clase5").style.backgroundColor = "green";
  }else{
    document.getElementById("clase5").style.backgroundColor = "white";
  }
  //Test si cambio la cantidad
  if( $('#clase6 li').size()>2){
    //alert('mas de dos');
    document.getElementById("clase6").style.backgroundColor = "green";
  }else{
    document.getElementById("clase6").style.backgroundColor = "white";
  }
  //Test si cambio la cantidad
  if( $('#clase7 li').size()>2){
    //alert('mas de dos');
    document.getElementById("clase7").style.backgroundColor = "green";
  }else{
    document.getElementById("clase7").style.backgroundColor = "white";
  }
  //Test si cambio la cantidad
  if( $('#clase8 li').size()>2){
    //alert('mas de dos');
    document.getElementById("clase8").style.backgroundColor = "green";
  }else{
    document.getElementById("clase8").style.backgroundColor = "white";
  }
  //Test si cambio la cantidad
  if( $('#clase9 li').size()>2){
    //alert('mas de dos');
    document.getElementById("clase9").style.backgroundColor = "green";
  }else{
    document.getElementById("clase9").style.backgroundColor = "white";
  }
  //Test si cambio la cantidad
  if( $('#clase10 li').size()>2){
    //alert('mas de dos');
    document.getElementById("clase10").style.backgroundColor = "green";
  }else{
    document.getElementById("clase10").style.backgroundColor = "white";
  }
  //Test si cambio la cantidad
  if( $('#clase11 li').size()>2){
    //alert('mas de dos');
    document.getElementById("clase11").style.backgroundColor = "green";
  }else{
    document.getElementById("clase11").style.backgroundColor = "white";
  }
  //Test si cambio la cantidad
  if( $('#clase12 li').size()>2){
    //alert('mas de dos');
    document.getElementById("clase12").style.backgroundColor = "green";
  }else{
    document.getElementById("clase12").style.backgroundColor = "white";
  }
  //Test si cambio la cantidad
  if( $('#clase13 li').size()>2){
    //alert('mas de dos');
    document.getElementById("clase13").style.backgroundColor = "green";
  }else{
    document.getElementById("clase13").style.backgroundColor = "white";
  }
  //Test si cambio la cantidad
  if( $('#clase14 li').size()>2){
    //alert('mas de dos');
    document.getElementById("clase14").style.backgroundColor = "green";
  }else{
    document.getElementById("clase14").style.backgroundColor = "white";
  }
  //Test si cambio la cantidad
  if( $('#clase15 li').size()>2){
    //alert('mas de dos');
    document.getElementById("clase15").style.backgroundColor = "green";
  }else{
    document.getElementById("clase15").style.backgroundColor = "white";
  }
  //Test si cambio la cantidad
  if( $('#clase16 li').size()>2){
    //alert('mas de dos');
    document.getElementById("clase16").style.backgroundColor = "green";
  }else{
    document.getElementById("clase16").style.backgroundColor = "white";
  }
  //Test si cambio la cantidad
  if( $('#clase17 li').size()>2){
    //alert('mas de dos');
    document.getElementById("clase17").style.backgroundColor = "green";
  }else{
    document.getElementById("clase17").style.backgroundColor = "white";
  }
  //Test si cambio la cantidad
  if( $('#clase18 li').size()>2){
    //alert('mas de dos');
    document.getElementById("clase18").style.backgroundColor = "green";
  }else{
    document.getElementById("clase18").style.backgroundColor = "white";
  }
  //Test si cambio la cantidad
  if( $('#clase19 li').size()>2){
    //alert('mas de dos');
    document.getElementById("clase19").style.backgroundColor = "green";
  }else{
    document.getElementById("clase19").style.backgroundColor = "white";
  }
  //Test si cambio la cantidad
  if( $('#clase20 li').size()>2){
    //alert('mas de dos');
    document.getElementById("clase20").style.backgroundColor = "green";
  }else{
    document.getElementById("clase20").style.backgroundColor = "white";
  }
  //Test si cambio la cantidad
  if( $('#clase21 li').size()>2){
    //alert('mas de dos');
    document.getElementById("clase21").style.backgroundColor = "green";
  }else{
    document.getElementById("clase21").style.backgroundColor = "white";
  }
  //Test si cambio la cantidad
  if( $('#clase22 li').size()>2){
    //alert('mas de dos');
    document.getElementById("clase22").style.backgroundColor = "green";
  }else{
    document.getElementById("clase22").style.backgroundColor = "white";
  }
  //Test si cambio la cantidad
  if( $('#clase23 li').size()>2){
    //alert('mas de dos');
    document.getElementById("clase23").style.backgroundColor = "green";
  }else{
    document.getElementById("clase23").style.backgroundColor = "white";
  }
  //Test si cambio la cantidad
  if( $('#clase24 li').size()>2){
    //alert('mas de dos');
    document.getElementById("clase24").style.backgroundColor = "green";
  }else{
    document.getElementById("clase24").style.backgroundColor = "white";
  }

}


//Exportar a PDF --------------------------------------------------------

bexport_pdf.addEventListener('click', () => {

          event.preventDefault();
          html2canvas(document.querySelector('#content'), {
              allowTaint: true,
              taintTest: false,
              onrendered: function(canvas) {
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth()+1; //January is 0!
                var yyyy = today.getFullYear();

                if(dd<10) {
                    dd = '0'+dd
                }

                if(mm<10) {
                    mm = '0'+mm
                }

                today = mm + '/' + dd + '/' + yyyy;
                // canvas is the final rendered <canvas> element
                var myImage = canvas.toDataURL("image/png");

                var pdf = jsPDF('landscape', 'pt', 'legal')
                //download(myImage, "dlDataUrlBin", "application/pdf")
                pdf.addImage(myImage, 'JPEG', 0, 0);
                pdf.save("cariograma_"+today+"_.pdf");
                //window.open(myImage);
              },
              height: 1200
          });

});

//----------------------------------------------------------------------------
//reset - Limpiar cariograma
let reset = document.querySelector('#reset')

reset.addEventListener('click',() => {
  location.reload(true);//force reload true
},false);
