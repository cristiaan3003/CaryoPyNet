const zerorpc = require("zerorpc")
let client = new zerorpc.Client({ timeout: 3000, heartbeatInterval: 1000000 })

//{ timeout: 3000, heartbeatInterval: 300000 }
//timeout=600000,heartbeatInterval=600000
client.connect("tcp://127.0.0.1:4242")
//var fs = require('fs');
//text = fs.readFileSync('server','utf8').toString().split('\n')
//client.connect(text[0])

client.invoke("echo", "server ready", (error, res) => {
  if(error || res !== 'server ready') {
    console.error(error)
  } else {
    console.log("server is ready")
    insertarLetrasAnimadas(inTurnFadingTextG);
    document.getElementById("inTurnFadingTextG").style.display = "none"
  }
})


//----------------------------------------------------------------------------------------

const {dialog} = require('electron').remote;

//--------------------
//seleccionar IMAGEN
let select_dir = document.querySelector('#select_dir')
let path_dir_img = document.querySelector('#path_dir_img')

/*select_img.addEventListener('click',function(){
    dialog.showOpenDialog(function (fileNames) {
        if(fileNames === undefined){
            console.log("No file selected");
        }else{
            path_file_img.value = fileNames[0];
        }
    });
},false);*/

select_dir.addEventListener('click',function(){
    let pathDirectory = dialog.showOpenDialog({properties: ['openDirectory']});
    if(pathDirectory === undefined){
        console.log("Directorio no seleccionado");
    }else{
          bdump_img.disabled=false
          path_dir_img.value = pathDirectory;
    }

},false);

//------------------------------------------------------------------
//resultado_clasifica
let bdump_img = document.querySelector('#bdump_img')
let tresultado_img = document.querySelector('#tresultado_img')
let ancho_resize = document.querySelector('#ancho_resize')
let alto_resize = document.querySelector('#alto_resize')
let agregar_a_dump = document.querySelector('#cbox_dump')

//insertar letras animadas
function insertarLetrasAnimadas(inTurnFadingTextG) {

    var elem1 = document.createElement("div");
    var elem2 = document.createElement("div");
    var elem3 = document.createElement("div");
    var elem4 = document.createElement("div");
    var elem5 = document.createElement("div");
    var elem6 = document.createElement("div");
    var elem7 = document.createElement("div");
    var elem8 = document.createElement("div");
    var elem9 = document.createElement("div");
    var elem10 = document.createElement("div");
    //procesando
    elem1.setAttribute("id", "inTurnFadingTextG_1");
    elem1.setAttribute("class", "inTurnFadingTextG");
    elem1.innerHTML="P"

    elem2.setAttribute("id", "inTurnFadingTextG_2");
    elem2.setAttribute("class", "inTurnFadingTextG");
    elem2.innerHTML="r"

    elem3.setAttribute("id", "inTurnFadingTextG_3");
    elem3.setAttribute("class", "inTurnFadingTextG");
    elem3.innerHTML="o"

    elem4.setAttribute("id", "inTurnFadingTextG_4");
    elem4.setAttribute("class", "inTurnFadingTextG");
    elem4.innerHTML="c"

    elem5.setAttribute("id", "inTurnFadingTextG_5");
    elem5.setAttribute("class", "inTurnFadingTextG");
    elem5.innerHTML="e"

    elem6.setAttribute("id", "inTurnFadingTextG_6");
    elem6.setAttribute("class", "inTurnFadingTextG");
    elem6.innerHTML="s"

    elem7.setAttribute("id", "inTurnFadingTextG_7");
    elem7.setAttribute("class", "inTurnFadingTextG");
    elem7.innerHTML="a"

    elem8.setAttribute("id", "inTurnFadingTextG_8");
    elem8.setAttribute("class", "inTurnFadingTextG");
    elem8.innerHTML="n"

    elem9.setAttribute("id", "inTurnFadingTextG_9");
    elem9.setAttribute("class", "inTurnFadingTextG");
    elem9.innerHTML="d"

    elem10.setAttribute("id", "inTurnFadingTextG_10");
    elem10.setAttribute("class", "inTurnFadingTextG");
    elem10.innerHTML="o"


    inTurnFadingTextG.appendChild(elem1);
    inTurnFadingTextG.appendChild(elem2);
    inTurnFadingTextG.appendChild(elem3);
    inTurnFadingTextG.appendChild(elem4);
    inTurnFadingTextG.appendChild(elem5);
    inTurnFadingTextG.appendChild(elem6);
    inTurnFadingTextG.appendChild(elem7);
    inTurnFadingTextG.appendChild(elem8);
    inTurnFadingTextG.appendChild(elem9);
    inTurnFadingTextG.appendChild(elem10);
}



bdump_img.addEventListener('click', () => {

  document.getElementById("inTurnFadingTextG").style.display = ""
  document.getElementById("circleloader").style.display = ""
  document.getElementById("checkmark").style.display = ""
  document.getElementById("circleloader").classList.remove('load-complete');
  bdump_img.disabled=true

  client.invoke("dumpRenderer",path_dir_img.value,ancho_resize.value,alto_resize.value,agregar_a_dump.checked, (error, salida) => {
    if(error) {
      console.error(error)
    } else {
      $('.circle-loader').toggleClass('load-complete');
      $('.checkmark').toggle();
      document.getElementById("inTurnFadingTextG").style.display = "none"
      tresultado_img.textContent = salida


    }
  })
})
