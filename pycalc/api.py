#Todo LO QUE importe aqui tiene que estar definida en calc.py
#sino no anda, no da ningun aviso de error
from __future__ import print_function
from calc import wrapper_recupera_clase_directorio as recuperar_clases
from calc import procesarTODOdump as dumpImageFolders
from calc import entrenarNet as entrenar
from calc import wrapper_extraerCaractCNN as extraer
from calc import entrenarELM as entrenar_elm
from calc import wrapper_enderezar_cromosomas as enderezar
from calc import getDeviceUse as getDevice

import sys
import zerorpc

class CalcApi(object):

    def getDev(self):
        try:
            return getDevice()
        except Exception as e:
            return e

    def recuperaClaseRenderer(self,pathDirectory):
        """based on the input, return the int result"""
        try:
            import os
            script_dir=os.getcwd()
            ##cargo los parametros de los archivos de la red --> cnn, elm y scaler

            pathELM=script_dir+"/data/net/elm.plk"
            pathScaler=script_dir+"/data/net/scaler.plk"
            pathNet=script_dir+"/data/net/net1.plk"
            if (os.path.isfile(pathScaler) and os.path.isfile(pathNet) and os.path.isfile(pathELM)):
                return recuperar_clases(pathDirectory,pathELM,pathScaler,pathNet)
            else:
                salida = 0

            return salida
        except Exception as e:
            return e

    def dumpRenderer(self,pathDirectory,ancho_resize,alto_resize,agregar_a_dump):
        """based on the input, return the int result"""
        try:
            return dumpImageFolders(pathDirectory,int(ancho_resize),int(alto_resize),agregar_a_dump)
        except Exception as e:
            return e

    def cargaWebRenderer(self):
        """based on the input, return the int result"""
        try:
            return real_cargaWeb()
        except Exception as e:
            return e

    def trainNetRenderer(sefl,learning_rate,momentum,numero_epocas, simular):
        try:
            import os
            script_dir = os.getcwd()
            test_siz=0.05
            return entrenar(script_dir,float(learning_rate),float(momentum),int(numero_epocas),float(test_siz),simular)
        except Exception as e:
            return e

    def extraerRenderer(sefl):
        try:
            import os
            script_dir = os.getcwd()
            extraer(script_dir)
            return "Finalizo Extraer"
        except Exception as e:
            return e

    def trainELMRenderer(sefl,neuronas,alpha,activation,simular):
        try:
            import os
            script_dir = os.getcwd()
            full_path_file_caract=script_dir+"/data/caract/caracteristicas.csv"
            test_siz=0.05
            score=entrenar_elm(script_dir,int(neuronas),float(alpha),str(activation),full_path_file_caract,float(test_siz),simular)
            return score
        except Exception as e:
            return e

    def enderezarRenderer(sefl,path_dir_cromosomas,path_dir_salida):
        try:
            import os
            import csv
            script_dir = os.getcwd()
            path_dump=script_dir+"/data/dump"
            rhs = path_dir_cromosomas.split("/");
            #guardar nombre del corpus
            text_file = open(path_dump+'/corpus_entrenado.txt', "w")
            text_file.write(rhs[-1])
            text_file.close()

            return enderezar(path_dir_cromosomas,path_dir_salida)
        except Exception as e:
            return e


    def configurarSimple(sefl,path_dir_cromosomas,learning_rate,momentum,numero_epocas,neuronas,alpha,activation):
        try:
            import os
            import csv
            import shutil
            script_dir = os.getcwd()
            path_dump=script_dir+"/data/dump"
            rhs = path_dir_cromosomas.split("/");
            path_dir_salida= script_dir+"/tmp/enderezados"
            if not os.path.exists(path_dir_salida):#sino existe
                #creo el directorio
                os.makedirs(path_dir_salida)



            enderezar(path_dir_cromosomas,path_dir_salida)
            dumpImageFolders(path_dir_salida,int(50),int(50),False)#False -> reinicia el dump (borra carpeta y crea de nuevo)
            #guardar nombre del corpus con el cual se entrenara la cnn y luego se extraen las caracteristias
            text_file = open(path_dump+'/corpus_entrenado.txt', "w")
            text_file.write(rhs[-1])
            text_file.close()

            test_siz=0.05
            #Entrena CNN
            entrenar(script_dir,float(learning_rate),float(momentum),int(numero_epocas),float(test_siz),True)
            #Cnn Extrae las Caracteristicas
            extraer(script_dir)
            #Reinicia carpetas enderezados- borro el directorio y lo creo de nuevo
            shutil.rmtree(path_dir_salida, ignore_errors=False, onerror=None)
            os.mkdir(path_dir_salida, 0o777)
            #Entrenar ELM
            full_path_file_caract=script_dir+"/data/caract/caracteristicas.csv"
            test_siz=0.02
            return entrenar_elm(script_dir,int(neuronas),float(alpha),str(activation),full_path_file_caract,float(test_siz),True)

        except Exception as e:
            return e

    def echo(self, text):
        """echo any text"""
        return text


def parse_port():
    port = 4242
    try:
        port = int(sys.argv[1])
    except Exception as e:
        pass
    return '{}'.format(port)

def main():
    addr = 'tcp://127.0.0.1:' + parse_port()
    s = zerorpc.Server(CalcApi())
    s.bind(addr)
    print('start running on {}'.format(addr))
    s.run()

if __name__ == '__main__':
    main()
