
from __future__ import print_function
import numpy as np
import math
import csv
from sklearn import model_selection
import time
import pickle
from lasagne import layers
import theano
from sklearn import preprocessing
from sklearn import metrics
from sklearn import linear_model
from sklearn_extensions.extreme_learning_machines.elm import ELMClassifier
import pandas
import lasagne
from lasagne.updates import nesterov_momentum
from nolearn.lasagne import NeuralNet
import cv2
import os
from scipy import ndimage
from skimage import measure
import copy
import matplotlib.pyplot as plt

#------------------------------------------------------------------------
#------------------------------------------------------------------------
#-------------------------EXTRAS-----------------------------------------
#------------------------------------------------------------------------
#------------------------------------------------------------------------

#getDeviceUSe
import getpass
def getDeviceUse():
	#get user name
	user_name=getpass.getuser()
        #/home/asusn56/.theanorc
	path="/home/"+user_name+"/.theanorc"
	#leer archivo
	text_file = open(path, "r")
	a=text_file.read()
	text_file.close()
	return a

def classifaction_report_csv(report,script_dir):
    report_data = []
    lines = report.split('\n')
    for line in lines[2:-3]:
        row = {}
        row_data = line.split('      ')
        row['class'] = row_data[0]
        row['precision'] = float(row_data[1])
        row['recall'] = float(row_data[2])
        row['f1_score'] = float(row_data[3])
        row['support'] = float(row_data[4])
        report_data.append(row)
    dataframe = pandas.DataFrame.from_dict(report_data)
    dataframe.to_csv(script_dir+'/tmp/classification_report.csv', index = False)



#load imagen
def load_image(full_path):
    img = cv2.imread(full_path)
    if len(img.shape)==3:
        img = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    return img

#count files and folder
def count_folders_files(path):
    files = folders = 0
    for _, dirnames, filenames in os.walk(path):
        files += len(filenames)
        folders += len(dirnames)
    return folders,files

#build rel_path
#rel_path->ejem:/single_clase/9700TEST.6.tiff/clase1/clase1_0_9700TEST.6.tiff
#retorna los path a los archivos de la clase que se la pase en clase_folder
def build_rel_path(pathDirectory,name_example_folder,clase_folder):
    rel_path=pathDirectory+"/"+name_example_folder+"/"+clase_folder
    num_files_folder=count_folders_files(rel_path)[1]
    rel_paths_img_clase=np.empty(num_files_folder, dtype='object')
    for i in range(0,len(rel_paths_img_clase)):
        rel_paths_img_clase[i]=rel_path+"/"+clase_folder+"_"+str(i)+"_"+name_example_folder
    return rel_paths_img_clase


#los ALL PATH del caso de estudio (path a las imagenes separadas por clase)
def load_all_PATH(pathDirectory,name_example_folder):
    path=pathDirectory+"/"+name_example_folder
    cantidad_de_clases=count_folders_files(path)[0]
    string_paths_img=np.empty(cantidad_de_clases, dtype='object')
    for j in range(0,cantidad_de_clases):
        clase_folder="clase"+str(j+1)
        rel_paths_img_clase=build_rel_path(pathDirectory,name_example_folder,clase_folder)
        string_paths_img[j]=rel_paths_img_clase
    return string_paths_img


def dumpImage(path_to_img,ancho_resize,alto_resize):
    img=load_image(path_to_img)
    img = cv2.resize(img, (ancho_resize, alto_resize))
    #print(img.shape)
    arr = np.array(img)
    # record the original shape
    shape = arr.shape
    # make a 1-dimensional view of arr
    flat_arr = arr.ravel()
    #Normalized Data
    flat_arr_norm=(flat_arr-min(flat_arr))/(max(flat_arr)-min(flat_arr))
    flat_arr_norm[flat_arr_norm==1.]=0.
    return flat_arr_norm

#=====================================================================================
#=====================================================================================
#Buscar indices FILA para cortar la parte blanca de la imagen ROI
def findStartIndex(img,h,w):
    for i in range(0, h):#para fila i
           aux=img[i:i+1,0:w]
           sum=math.fsum(aux[0])
           if 255*w!=sum:
                return i-1

def findEndIndex(img,h,w):
    for i in reversed(range(0, h)):#para fila i
           aux=img[i:i+1,0:w]
           sum=math.fsum(aux[0])
           if 255*w!=sum:
                return i+1

def findStartEndFila(img):
    h,w=img.shape
    index1=findStartIndex(img,h,w)
    index2=findEndIndex(img,h,w)
    return index1,index2

#=====================================================================================
#=====================================================================================
#Buscar indices COLUMNA para cortar la parte blanca de la imagen ROI
def findStartIndexCol(img,h,w):
    for i in range(0, w):
           aux=img[0:h,i:i+1]
           sum=math.fsum(np.transpose(aux)[0])
           if 255*h!=sum:
                return i-1

def findEndIndexCol(img,h,w):
    for i in reversed(range(0, w)):
           aux=img[0:h,i:i+1]
           sum=math.fsum(np.transpose(aux)[0])
           if 255*h!=sum:
                return i+1

def findStartEndColumna(img):
    h,w=img.shape
    index1=findStartIndexCol(img,h,w)
    index2=findEndIndexCol(img,h,w)
    return index1,index2

#=====================================================================================
#=====================================================================================

#ELIMINA OBJETOS DE TAMAÑO menor a "tamano" EN LA IMAGEN
def eliminar_objetos_pequeños(th,tamano):
    blobs_labels = measure.label(th, background=255)
    boxes_objetos= ndimage.find_objects(blobs_labels)
    #print(boxes_objetos[0][0])
    #ejemplo1
    #(slice(0, 2, None), slice(19, 23, None))
    #So this object is found at x = 19–23 and y = 0–2
    if len(boxes_objetos)>1:#solo debe haber 1 objeto->el cromosoma (todos los objetos mas pequeños a un cromosomas se eliminan)
        #filtro objetos muy pequeños ya que son objetos indeseados (no cromosomas)
        aux_boxes=[]
        for l in range(0,len(boxes_objetos)):
            if len(np.transpose(np.nonzero(cv2.bitwise_not(th[boxes_objetos[l]]))))<tamano:
                th[boxes_objetos[l]]=255
    return th

#RELLENA HUECOS
def floodfill(th):
    # Copy the thresholded image.
    im_floodfill = th.copy()

    # Mask used to flood filling.
    # Notice the size needs to be 2 pixels than the image.
    h, w = th.shape[:2]
    mask = np.zeros((h+2, w+2), np.uint8)

    # Floodfill from point (0, 0)
    cv2.floodFill(im_floodfill, mask, (0,0), 255);

    # Invert floodfilled image
    im_floodfill_inv = cv2.bitwise_not(im_floodfill)

    # Combine the two images to get the foreground.
    im_out = th | im_floodfill_inv

    return im_out

#Obtine informacion de Area y perimetro de un cromosoma
#Recibe como parametro el imagen binaria del cromosoma
def contorno(thresh):
    _, contours, _ = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    imagen_contorno= np.zeros(thresh.shape,np.uint8)
    cv2.drawContours(imagen_contorno, contours, -1, 255, 1)
    perimetro_cromosoma = cv2.arcLength(contours[0],True)#perimetro del contorno
    moments = cv2.moments(contours[0])
    area_cromosoma = moments['m00'] ##area del cromosoma --> igual a area = cv2.contourArea(cnt)
    return area_cromosoma,perimetro_cromosoma,imagen_contorno


#calcular area y perimetro de un cromosoma
def calcular_area_perimetro(full_path_img_j_i):
        img=load_image(full_path_img_j_i)#img -> gray
        gray=cv2.copyMakeBorder(img.copy(), top=1, bottom=1, left=1, right=1, borderType= cv2.BORDER_CONSTANT, value=[255,255,255] )
        ret, thresh = cv2.threshold(gray,250,255,cv2.THRESH_BINARY)
        thresh=eliminar_objetos_pequeños(thresh,20) #eliminar objetos pequeños
        thresh=floodfill(cv2.bitwise_not(thresh)) #rellenar huecos
        area_cromosoma,perimetro_cromosoma,imagen_contorno=contorno(thresh)
        return area_cromosoma,perimetro_cromosoma,imagen_contorno,img

#calcular Areas y Perimetros de todos los cromosomas de la clase
def calcular_areasCromo_perimsContornoCromo(string_path_img):
    perimetros=[]
    areas=[]
    from joblib import Parallel, delayed
    import multiprocessing
    num_cores = multiprocessing.cpu_count()
    for j in range(0,len(string_path_img)):#j->clase
        areaX=[]
        perimetroX=[]
        for i in range(0,len(string_path_img[j])):#i ->img dentro de la clase
            full_path_img_j_i=string_path_img[j][i]
            area_cromosoma,perimetro_cromosoma,imagen_contorno,_=calcular_area_perimetro(full_path_img_j_i)
            areaX.append(area_cromosoma)
            perimetroX.append(perimetro_cromosoma)
        areas.append(areaX)
        perimetros.append(perimetroX)
    return areas,perimetros

#normaliza vector (multidimensional) de perimetro y  area
#normalizar [MAX MIN] to [0,1]
def normalizar_vector(vector):
    vector_normalizado=copy.deepcopy(vector)
    import operator
    flat_list=[item for sublist in vector for item in sublist]
    min_index, min_value = min(enumerate(flat_list), key=operator.itemgetter(1))
    max_index, max_value = max(enumerate(flat_list), key=operator.itemgetter(1))
    #print(flat_list)
    for i in range(0,len(vector)):
        for j in range(0,len(vector[i])):
            #print(vector_normalizado[i][j])
            if max_value!=min_value:
                vector_normalizado[i][j]=((vector[i][j]-min_value)/(max_value-min_value))
            else:
                vector_normalizado.append(1)
    return vector_normalizado

#------------------------------------------------------------------------
#------------------------------------------------------------------------
#-------------------------DUMP-----------------------------------
#------------------------------------------------------------------------
#------------------------------------------------------------------------

#Procesa Todos los cromosomas de una carpeta
def procesarCarpeta(pathDirectory,path_dump,name_example_folder,ancho_resize,alto_resize):

    string_path_img=load_all_PATH(pathDirectory,name_example_folder)
    for j in range(0,len(string_path_img)):#j->clase
        for i in range(0,len(string_path_img[j])):#i ->img dentro de la clase
            flat_arr_norm=dumpImage(string_path_img[j][i],ancho_resize,alto_resize)
            #‘w’ – Write mode which is used to edit and write new information to the file (any existing files with the same name will be erased when this mode is activated)
            #‘a’ – Appending mode, which is used to add new data to the end of the file; that is new information is automatically amended to the end
            file = open(path_dump+"/clase"+str(j)+".csv", 'a')
            wr = csv.writer(file, quoting=csv.QUOTE_MINIMAL)
            wr.writerow(flat_arr_norm)
            file.close()


#Procesa todas las carpetas
def procesarTODOdump(pathDirectory,ancho_resize,alto_resize,agregar_a_dump):
        lista_name_example_folder=[name for name in os.listdir(pathDirectory)]
        script_dir = os.getcwd()
        path_dump=script_dir+"/data/dump"
        if not os.path.exists(path_dump):
            #creo el directorio
            os.makedirs(path_dump)

        #Reinicia el Dump- borro el directorio y lo creo de nuevo
        if not agregar_a_dump:
            import shutil
            shutil.rmtree(path_dump, ignore_errors=False, onerror=None)
            os.mkdir(path_dump, 0o777)

        #guardar ancho y alto ingresado
        #(NOTA: reescribo cada vez que se ejecuta
        #recordar: si se agrega datos al dump se debe usar el mismo ancho y alto que en el dump existente)
		#el ancho y alto del resize se fijo a 50x50 en el almacenado de cromosomas (dump.js)
        file = open(path_dump+"/ancho_alto.csv", 'w')
        wr = csv.writer(file, quoting=csv.QUOTE_MINIMAL)
        wr.writerow([ancho_resize,alto_resize])
        file.close()

        #========================================================================
        #Procesa todas las carpetas dentro del directorio
        for i in range(0,len(lista_name_example_folder)):#-->Prosesa una carpeta a la vez
          procesarCarpeta(pathDirectory,path_dump,lista_name_example_folder[i],ancho_resize,alto_resize)
        return "Finalizo Dump"



#------------------------------------------------------------------------
#------------------------------------------------------------------------
#-------------------------CLASIFICAR-----------------------------------
#------------------------------------------------------------------------
#------------------------------------------------------------------------


def recupera_clase_imagen(img,f_dense,scaler,elm,ancho_resize,alto_resize):

    #DumpIMage pasando la imagen - Sin path
    img = cv2.resize(img, (ancho_resize, alto_resize))
    arr = np.array(img)
    shape = arr.shape
    flat_arr = arr.ravel()
    flat_arr_norm=(flat_arr-min(flat_arr))/(max(flat_arr)-min(flat_arr))
    flat_arr_norm[flat_arr_norm==1.]=0.

    X = np.array(flat_arr_norm)
    X = X.astype(np.float32)
    X_ = X.reshape((-1, 1, ancho_resize, alto_resize))

    out_dense = f_dense(X_)
    out_dense = out_dense[0]
    sX_test = scaler.transform(out_dense.reshape(1, -1)) # [ [...] ]
    sX_test.mean(axis=0)
    Y_pred = elm.predict(sX_test)
    ###!!!!! RETURN +1 para pasar a una numeracion default -> las clases estan numeradas de [0,23]-> [1,24]
    return int(Y_pred[0])+1

def wrapper_recupera_clase_directorio(pathDirectory,pathELM,pathScaler,pathNet):

    script_dir=os.getcwd()
    #Check is all necesary files exist
    #os.path.isfile(file_path)
    salida=[]
    if not os.path.exists(script_dir+"/tmp"):
        #creo el directorio para archivos temporales
        os.makedirs(script_dir+"/tmp")

    #coloco en una lista los nombres de las imagenes que estan
    #de la carpeta selecccionada
    lista_name_example_folder=[name for name in os.listdir(pathDirectory) ]

    #full paths de las imagenes a clasificar
    paths=[]
    for dirpath,_,filenames in os.walk(pathDirectory):
           for f in filenames:
             paths.append(os.path.abspath(os.path.join(dirpath, f)))

    perimetros=[]
    areas=[]
    for i in range(0,len(paths)):
        _,perimetro_cromosoma,_,_=calcular_area_perimetro(paths[i])
        perimetros.append(perimetro_cromosoma)

    #copia_perim=copy.deepcopy(perimetros)
    import operator
    min_index, min_value = min(enumerate(perimetros), key=operator.itemgetter(1))
    max_index, max_value = max(enumerate(perimetros), key=operator.itemgetter(1))



    pathDirectoryDump=script_dir+"/data/dump/" #path dum
    val=pandas.read_csv(pathDirectoryDump+'ancho_alto.csv', sep=',',header=None, names=['f'+str(m) for m in range(0,2)])
    ancho_resize=int(np.array(val['f0'])[0])
    alto_resize=int(np.array(val['f1'])[0])

    scaler=pickle.load(open(pathScaler, "rb" ))
    elm=pickle.load(open(pathELM, "rb" ))
    net=pickle.load(open(pathNet, "rb" ))

    #cargo net cnn- capa densa
    dense_layer = layers.get_output(net.layers_['dense'], deterministic=True)
    input_var = net.layers_['input'].input_var
    f_dense = theano.function([input_var], dense_layer)

    for i in range(0,len(paths)):

		#Acutalizo la etiqueta de informacion para el usuario --> Enderezando cromosoma
		#si el archivo de caracteristicas existe lo sobre escribo
        file = open(script_dir+'/tmp/info_generando_cario','w')
        file.write("Enderezando Cromosoma")
        file.close()

        #---pathImg=pathDirectory+"/"+lista_name_example_folder[i]
        _,perimetro_cromosoma,_,img=calcular_area_perimetro(paths[i])
        perim_normalizado=((perimetro_cromosoma-min_value)/(max_value-min_value))
        dst1=enderezar(img,perim_normalizado)
        indexsF=findStartEndFila(dst1)
        indexsC=findStartEndColumna(dst1)
        enderezado=dst1[indexsF[0]:indexsF[1],indexsC[0]:indexsC[1]]

        #img=load_image(paths[i])

        split_path=paths[i].split("/")
        split_extension=split_path[-1].split(".tiff")##los navegadores no renderizan .tiff,.tif
        tmp_path=script_dir+"/tmp"+split_extension[0]+".png" ##imagen temporal que sera renderizada por el navegador
        cv2.imwrite(tmp_path,img)##escribo imagen temporal
        import base64
        with open(tmp_path,'rb') as fp:
            image = base64.b64encode(fp.read())
            encoded_image = "data:image/png;base64," + str(image)[2:-1]
        os.remove(tmp_path)
        file = open(script_dir+'/tmp/info_generando_cario','w')
        file.write("Clasificando")
        file.close()
        clase_calculada=recupera_clase_imagen(enderezado,f_dense,scaler,elm,ancho_resize,alto_resize)

        salida.append([clase_calculada,encoded_image])
        file = open(script_dir+'/tmp/info_generando_cario','w')
        file.write("Inicializando...")
        file.close()

    return salida


#------------------------------------------------------------------------
#------------------------------------------------------------------------
#-------------------------ENTRENAR CNN-----------------------------------
#-------------------------ENTRENAR CNN-----------------------------------
#------------------------------------------------------------------------
#------------------------------------------------------------------------


def read_csv_files(folder, Nfeatures):
    clase = dict()

    for n in range(0,24):

      clase[n] = pandas.read_csv(folder +'clase' + str(n) + '.csv', sep=',',header=None, names=['f'+str(m) for m in range(0,Nfeatures)])
#        clase[n+1] = np.array(pandas.read_csv(folder +'clase' + str(n+1) + '.csv', sep=',',header=None, names=['f'+str(m) for m in range(0,Nfeatures)]))
    return clase

def load_clases(pathDirectory,ancho_resize,alto_resize):
    clase = read_csv_files(pathDirectory,ancho_resize*alto_resize)

    clases_a_usar = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]

    X = []
    Y = []

    for idx in clases_a_usar:

        x = np.array(clase[idx]).tolist()
        y = idx * np.ones((clase[idx].shape[0]))
        y = y.tolist()

        X.extend(x)
        Y.extend(y)

    clase=[]
    return X,Y

def particionar(X,Y,test_siz,ancho_resize,alto_resize):

    X_train, X_test, Y_train, Y_test = model_selection.train_test_split(X,Y,test_size=test_siz,random_state=0)

    X_train = np.array(X_train)
    X_train = X_train.astype(np.float32)

    Y_train = np.array(Y_train)
    Y_train = Y_train.astype(np.float32)

    X_train = X_train.reshape((-1, 1, ancho_resize, alto_resize))
    Y_train = Y_train.astype(np.uint8)


    #-----------------------------------

    X_test = np.array(X_test)
    X_test = X_test.astype(np.float32)

    Y_test = np.array(Y_test)
    Y_test = Y_test.astype(np.float32)

    X_test = X_test.reshape((-1, 1, ancho_resize, alto_resize))
    Y_test = Y_test.astype(np.uint8)


    return X_train, X_test, Y_train, Y_test

def fitNet(script_dir,X_train, X_test, Y_train, Y_test ,learning_rate,momentum,numero_epocas,simular,ancho_resize,alto_resize):

    net1 = NeuralNet(
        layers=[('input', layers.InputLayer),
                ('conv2d1', layers.Conv2DLayer),
                ('maxpool1', layers.MaxPool2DLayer),
                ('conv2d2', layers.Conv2DLayer),
                ('maxpool2', layers.MaxPool2DLayer),
                ('dropout1', layers.DropoutLayer),
                ('dense', layers.DenseLayer),
                ('dropout2', layers.DropoutLayer),
                ('output', layers.DenseLayer),
                ],
        # input layer
        input_shape=(None, 1, ancho_resize, alto_resize),
        # layer conv2d1
        conv2d1_num_filters=32,
        conv2d1_filter_size=(5, 5),
        conv2d1_nonlinearity=lasagne.nonlinearities.rectify,
        conv2d1_W=lasagne.init.GlorotUniform(),
        # layer maxpool1
        maxpool1_pool_size=(2, 2),
        # layer conv2d2
        conv2d2_num_filters=32,
        conv2d2_filter_size=(5, 5),
        conv2d2_nonlinearity=lasagne.nonlinearities.rectify,
        # layer maxpool2
        maxpool2_pool_size=(2, 2),
        # dropout1
        dropout1_p=0.5,
        # dense
        dense_num_units=128,
        dense_nonlinearity=lasagne.nonlinearities.rectify,
        # dropout2
        dropout2_p=0.5,
        # output
        output_nonlinearity=lasagne.nonlinearities.softmax,
        output_num_units=24,
        # optimization method params
        update=nesterov_momentum,
        update_learning_rate=learning_rate,
        update_momentum=momentum, ##lasagne.updates.nesterov_momentum(loss_or_grads, params, learning_rate, momentum=0.9)
        max_epochs=numero_epocas,
        verbose=1,
        )

    # Train the network
    net1.fit(X_train, Y_train)

    Y_pred=net1.predict(X_test)

    '''target_names = ['class 0', 'class 1', 'class 2', 'class 3', 'class 4', 'class 5', 'class 6', 'class 7',
                    'class 8', 'class 9', 'class 10', 'class 11', 'class 12', 'class 13', 'class 14',
                    'class 15', 'class 16', 'class 17', 'class 18', 'class 19', 'class 20', 'class 21',
                    'class 22', 'class 23']
    print("classification_report")
    print(metrics.classification_report(Y_test, preds, target_names=target_names))



    #===================
    # CONFUSSION MATRIX
    #===================
    print('\nCONFUSSION MATRIX')
    cm=metrics.confusion_matrix(Y_test,preds)
    plt.matshow(cm)
    plt.title('Matriz de confusión')
    plt.colorbar()
    plt.ylabel('Valor real')
    plt.xlabel('Valor predicho')
    plt.show()'''

	#Genera y guarda como imagen la matriz de confusion
    cm=metrics.confusion_matrix(Y_test,Y_pred)
    plt.matshow(cm)
    plt.title('Matriz de confusión')
    plt.ylabel('Valor Real')
    plt.xlabel('Valor Predicho')
    plt.savefig('tmp/confusion_matrix_cnn.png')

    score=net1.score(X_test,Y_test)
    if simular: #si es verdadero borro la vieja net y escribo la nueva
        path_net=script_dir+"/data/net"
        import sys
        sys.setrecursionlimit(100000)
        if not os.path.exists(path_net): # si no existe el path lo crea
            os.makedirs(path_net)
        pickle.dump(net1,open(path_net+"/net1.plk", "wb"))
        net1.save_params_to(path_net+'/net1.cfg')
    return score


def entrenarNet(script_dir,learning_rate,momentum,numero_epocas,test_siz,simular):
    pathDirectoryDump=script_dir+"/data/dump/" #path dum
    val=pandas.read_csv(pathDirectoryDump+'ancho_alto.csv', sep=',',header=None, names=['f'+str(m) for m in range(0,2)])
    ancho_resize=int(np.array(val['f0'])[0])
    alto_resize=int(np.array(val['f1'])[0])
    X,Y = load_clases(pathDirectoryDump,ancho_resize,alto_resize)
    X_train, X_test, Y_train, Y_test = particionar(X,Y,test_siz,ancho_resize,alto_resize)
    score=fitNet(script_dir,X_train, X_test, Y_train, Y_test ,learning_rate,momentum,numero_epocas,simular,ancho_resize,alto_resize)

    return score



#-----------------------------------------------------------
#-----------------------------------------------------------
#-------------- EXTRAER CARACTERISTICAS---------------------
#-----------------------------------------------------------
#-----------------------------------------------------------



def cargarDump(pathDirectoryDump,clases_a_usar,ancho_resize,alto_resize):

    clase = read_csv_files(pathDirectoryDump,ancho_resize*alto_resize)
    X = []
    Y = []

    for idx in clases_a_usar:

        x = np.array(clase[idx]).tolist()
        y = idx * np.ones((clase[idx].shape[0]))
        y = y.tolist()

        X.extend(x)
        Y.extend(y)

    clase=[]
    X = np.array(X)
    X = X.astype(np.float32)
    Y = np.array(Y)
    Y = Y.astype(np.float32)


    X_ = X.reshape((-1, 1, ancho_resize, alto_resize))
    Y_ = Y.astype(np.uint8)

    return X_,Y_,len(X)

def extraerCaractCNN(X_,Y_,largo,f_dense,myfile,ancho_resize,alto_resize):


    #Tomar una imagen del Set de Test y obtener su salida en la dense_layer

    for i in range(0,largo):
        instance = X_[i][None,:,:]
        print("-----")
        #print(instance[0])
        out_dense = f_dense(instance) #salida de la capa densa
        out_dense=out_dense[0]
        out_dense=out_dense.tolist()#de numpy a list
        out_dense.append(Y_[i])#agrego y_true en el final
        wr = csv.writer(myfile, quoting=csv.QUOTE_MINIMAL) #escribo a archivo
        wr.writerow(out_dense)

def wrapper_extraerCaractCNN(script_dir):

    #si no existe el directorio "caract" lo creo
    if not os.path.exists(script_dir+"/data/caract/"):
        os.makedirs(script_dir+"/data/caract/")
    #Defino paths
    pathDirectoryDump=script_dir+"/data/dump/"#archivos donde estan los volcados de las imagenes
    full_path_CNN=script_dir+"/data/net/net1.plk"# archivo de la cnn entrenada
    full_pathCaract=script_dir+"/data/caract/caracteristicas.csv"#archivo donde guardo caracteristicas
    val=pandas.read_csv(pathDirectoryDump+'ancho_alto.csv', sep=',',header=None, names=['f'+str(m) for m in range(0,2)])
    ancho_resize=int(np.array(val['f0'])[0])
    alto_resize=int(np.array(val['f1'])[0])


    clases_a_usar = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]

    #Cargar la CNN entrenada
    net1=pickle.load(open( full_path_CNN, "rb" ))
    #Theano layer functions and Feature Extraction
    dense_layer = layers.get_output(net1.layers_['dense'], deterministic=True)
    #output_layer = layers.get_output(net1.layers_['output'], deterministic=True)
    input_var = net1.layers_['input'].input_var
    #f_output = theano.function([input_var], output_layer)
    f_dense = theano.function([input_var], dense_layer)


    #si el archivo de caracteristicas existe lo sobre escribo
    myfile = open(full_pathCaract, 'w')

    X_,Y_,largo=cargarDump(pathDirectoryDump,clases_a_usar,ancho_resize,alto_resize)

    extraerCaractCNN(X_,Y_,largo,f_dense,myfile,ancho_resize,alto_resize)



#-------------------------------------------------------------------------
#-------------------------------------------------------------------------
#-------------------ENTRENAR ELM------------------------------------------
#-------------------------------------------------------------------------
#-------------------------------------------------------------------------

def entrenarELM(script_dir,n_h,alp,activation,full_path_file_caract,test_siz,simular):
    #load data
    with open(full_path_file_caract) as csvfile:
            readCSV = csv.reader(csvfile, delimiter=',')
            lines = list(readCSV)
            linea=np.transpose(lines)
            X=np.transpose(linea[:-1])
            Y=linea[-1]

    X_train, X_test, Y_train, Y_test = model_selection.train_test_split(X,Y,test_size=0.1,random_state=0)

    scaler = preprocessing.MinMaxScaler(feature_range=(0,1)).fit(X_train)

    sX_train = scaler.transform(X_train)
    sX_test = scaler.transform(X_test)

    sX_train.mean(axis=0)

    TRAIN = sX_train # --> Datos ESTANDARIZADOS
    TEST = sX_test

    elm=ELMClassifier(n_hidden=n_h,alpha=alp,activation_func=activation,activation_args=None,
                  user_components=None,regressor=linear_model.Ridge(),random_state=None)

    elm.fit(TRAIN,Y_train)
    Y_pred=elm.predict(TEST)


    target_names = ['class 0', 'class 1', 'class 2', 'class 3', 'class 4', 'class 5', 'class 6', 'class 7',
                    'class 8', 'class 9', 'class 10', 'class 11', 'class 12', 'class 13', 'class 14',
                    'class 15', 'class 16', 'class 17', 'class 18', 'class 19', 'class 20', 'class 21',
                    'class 22', 'class 23']

    report = metrics.classification_report(Y_test, Y_pred, target_names=target_names)
    classifaction_report_csv(report,script_dir)
    #Genera y guarda como imagen la matriz de confusion
    #cm=metrics.confusion_matrix(Y_train,Y_pred)
    cm=metrics.confusion_matrix(Y_test,Y_pred)
    #fig = plt.figure()
    plt.matshow(cm)

    plt.title('Matriz de confusión')
    plt.ylabel('Valor Real')
    plt.xlabel('Valor Predicho')
    plt.colorbar()
    plt.savefig(script_dir+'/tmp/confusion_matrix.png')
    score = elm.score(TEST, Y_test)

    if simular:
        pickle.dump(elm, open( script_dir+"/data/net/elm.plk", "wb" ) )
        pickle.dump(scaler, open( script_dir+"/data/net/scaler.plk", "wb" ) )

    return score


#-------------------------------------------------------------------------
#-------------------------------------------------------------------------
#-------------------ENDEREZAR CROMOSOMAS----------------------------------
#-------------------------------------------------------------------------
#-------------------------------------------------------------------------

#proyeccion horizontal (imagen de entrada=binaria)
def proyeccion_v(img,grados):
    filas,columnas= img.shape
    M = cv2.getRotationMatrix2D((columnas/2,filas/2),grados,1)
    mask_blanca=cv2.bitwise_xor(img,cv2.bitwise_not(img))
    dst_mask = cv2.warpAffine(mask_blanca,M,(columnas,filas))
    ret, mask = cv2.threshold(dst_mask,190,255,cv2.THRESH_BINARY)
    dst = cv2.warpAffine(img,M,(columnas,filas))
    ret, rot_thresh = cv2.threshold(dst,190,255,cv2.THRESH_BINARY)
    dst=cv2.bitwise_not(cv2.bitwise_xor(rot_thresh,mask))
    proy_v=[]
    sum=0
    for i in range(0, columnas):
       for j in range(0,filas):
                if dst[j][i]==0:
                    sum=sum+1
       proy_v.append(sum)
       sum=0
    return proy_v


#calcula las proyecciones verticales
#retorna una lista con las proyecciones verticales
def calcular_proyecciones_v(brazo1,brazo2,sentido):
    proyecciones_b1=[]
    proyecciones_b2=[]
    b1_fila,b1_columna=brazo1.shape
    b2_fila,b2_columna=brazo2.shape
    if sentido: #si True-> #girar en sentido horario el brazo
        for i in range(-5,13):
            ang_i=-i*5
            proy_v=proyeccion_v(brazo1,ang_i)
            proyecciones_b1.append((proy_v,ang_i,np.count_nonzero(proy_v)))
        for i in range(-5,13):
            ang_i=i*5
            proy_v=proyeccion_v(brazo2,ang_i)
            proyecciones_b2.append((proy_v,ang_i,np.count_nonzero(proy_v)))
    else:  #si False-> #girar en sentido horario el brazo superior
        for i in range(-5,13): #braso superior
            ang_i=i*5
            proy_v=proyeccion_v(brazo1,ang_i)
            proyecciones_b1.append((proy_v,ang_i,np.count_nonzero(proy_v)))
        for i in range(-5,13):
            ang_i=-i*5
            proy_v=proyeccion_v(brazo2,ang_i)
            proyecciones_b2.append((proy_v,ang_i,np.count_nonzero(proy_v)))

    return proyecciones_b1,proyecciones_b2

def mejor_proyeccion_v(p_b1,p_b2):
    valores=[]

    #brazo1
    for i in range(0,len(p_b1)):
        valores.append(p_b1[i][2])
    import operator
    min_index, min_value = min(enumerate(valores), key=operator.itemgetter(1))
    ang1=p_b1[min_index][1]

    #brazo2
    valores=[]
    for i in range(0,len(p_b2)):
        valores.append(p_b2[i][2])
    min_index, min_value = min(enumerate(valores), key=operator.itemgetter(1))
    ang2=p_b2[min_index][1]
    return ang1,ang2



#----------------------------------
#----------------------------------
def proyeccion_h2(gray,grados):
    ret, th = cv2.threshold(gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_TRIANGLE)
    th=cv2.bitwise_not(floodfill(cv2.bitwise_not(th))) #rellenar huecos (si quedo alguno)
    th=eliminar_objetos_pequeños(th,20) # si quedo algun pixel suelto

    filas,columnas= th.shape
    mask_blanca=cv2.bitwise_xor(th,cv2.bitwise_not(th))
    M = cv2.getRotationMatrix2D((columnas/2,filas/2),grados,1)
    dst = cv2.warpAffine(th,M,(columnas,filas))
    gray_rot=cv2.warpAffine(gray,M,(columnas,filas))
    dst_mask = cv2.warpAffine(mask_blanca,M,(columnas,filas))
    ret, rot_thresh = cv2.threshold(dst,190,255,cv2.THRESH_BINARY)
    ret, mask = cv2.threshold(dst_mask,190,255,cv2.THRESH_BINARY)


    dst=cv2.bitwise_not(cv2.bitwise_xor(rot_thresh,mask))
    dst_gray=cv2.bitwise_not(cv2.bitwise_and(cv2.bitwise_not(dst),cv2.bitwise_not(gray_rot)))

    proy_h=[]
    sum=0
    for i in range(0, filas):
           for j in range(0,columnas):
                    if dst[i][j]==0: #cuento si es pixel negro
                        sum=sum+1
           proy_h.append(sum)
           sum=0
    return proy_h,dst,dst_gray


#calcula las proyecciones horizontales entre ... y ... grados
#retorna una lista con las proyecciones horizontales
def calcular_proyecciones(gray):
    proyecciones=[]
    for i in range(-25,25): # con paso de 5 grados
        ang_i=i*5
        proy_h,dst,dst_gray=proyeccion_h2(gray,ang_i)
        proyecciones.append((proy_h,ang_i,dst,dst_gray))
    return proyecciones


def analisis2(proyecciones):
    vector_min=[]
    S=[]
    #print(proyecciones)
    for i in range(0,len(proyecciones)):
        proy_h_i=proyecciones[i][0]
        angulo=proyecciones[i][1]

        #contar ceros
        count=0
        for j in range(0,len(proy_h_i)):
            if proy_h_i[j]==0:
                count=count+1
            else:
                break
        offset=count+6
        count=0
        proy_h_i_offset=proy_h_i[offset:len(proy_h_i)-offset]
        if len(proy_h_i_offset)==0:
            offset=count+2
            proy_h_i_offset=proy_h_i[offset:len(proy_h_i)-offset]
        minimo=proy_h_i_offset[0]
        index_min=0
        for kk in range(0,len(proy_h_i_offset)):
            if minimo>proy_h_i_offset[kk]:
                minimo=proy_h_i_offset[kk]
                index_min=kk
        if index_min!=0 and index_min!=len(proy_h_i_offset)-1: #si pasa esto no girar la imagen, no hacer nada
            #partir
            izquierda=proy_h_i_offset[0:index_min]
            derecha=proy_h_i_offset[index_min:len(proy_h_i_offset)-1]

            #izquierda
            maximo_izq=izquierda[0]
            index_izq=0
            for ii in range(0,len(izquierda)):
                if maximo_izq<izquierda[ii]:
                    maximo_izq=izquierda[ii]
                    index_izq=ii

            #derecha
            maximo_der=derecha[0]
            index_der=0
            for ii in range(0,len(derecha)):
                if maximo_der<derecha[ii]:
                    maximo_der=derecha[ii]
                    index_der=ii
            index_der=index_der+index_min


            if index_izq!=0 and index_izq < index_min and index_min< index_der and index_der!=len(proy_h_i_offset)-1:
                R1=abs(proy_h_i_offset[index_izq]-proy_h_i_offset[index_der])/(proy_h_i_offset[index_izq]+proy_h_i_offset[index_der])
                R2=proy_h_i_offset[index_min]/(proy_h_i_offset[index_izq]+proy_h_i_offset[index_der])
                S_i=0.42*R1+0.54*R2
                #print(S_i)
                S.append((S_i,angulo,offset,index_izq,index_min,index_der))

    if len(S)!=0: #
        #print(S)
        S_min=S[0][0]
        S_index=0
        for i in range(0,len(S)):
            if S_min>S[i][0]:
                S_min=S[i][0]
                S_index=i
        #print(S_index)
        for i in range(0,len(proyecciones)):
            if S[S_index][1]==proyecciones[i][1]:
                dst=proyecciones[i][2]
                dst_gray=proyecciones[i][3]
            #cv2.namedWindow('imagen'+str(i), cv2.WINDOW_NORMAL)
            #cv2.imshow('imagen'+str(i),dst)
            #cv2.waitKey(0)
        return S[S_index],dst,dst_gray
    else:#si vacio considero el mejor ajuste de la imagen el actual, por lo tanto no la modifico. le dejo =
        return -1,-1,-1#si S==0 dejo la imagen como esta!

def sentido_giro(dst,index_min,offset,filas,columnas):
    aux=np.zeros((filas,columnas),np.uint8)
    aux=cv2.bitwise_not(aux)
    cv2.line(aux,(0,index_min+offset),(columnas,index_min+offset),0,1)
    aux=cv2.bitwise_not(aux)
    aux2=cv2.bitwise_and(aux,dst)#marco la linea
    aux3=aux2[index_min+offset:index_min+offset+1, 0:columnas] #recorto exactamente la linea
    no_zero_elem= np.nonzero(cv2.bitwise_not(aux3))
    posicion=(no_zero_elem[1][0]+no_zero_elem[1][len(no_zero_elem[1])-1])/2

    if posicion>(columnas/2): #girar en sentido horario el brazo superior, antihorario el inferior
        return True
    else: #girar en sentido horario el  brazo inferior, antihorario el superior
        return False

def girar_pegar_brazos(brazo1,brazo2,brazo1_gray,brazo2_gray,ang1,ang2):
    #brazo1
    filas,columnas= brazo1.shape
    mask_blanca=cv2.bitwise_xor(brazo1,cv2.bitwise_not(brazo1))
    M = cv2.getRotationMatrix2D((columnas/2,filas/2),ang1,1)
    dst = cv2.warpAffine(brazo1,M,(columnas,filas))
    gray_rot=cv2.warpAffine(brazo1_gray,M,(columnas,filas))
    dst_mask = cv2.warpAffine(mask_blanca,M,(columnas,filas))
    ret, rot_thresh = cv2.threshold(dst,254,255,cv2.THRESH_BINARY)
    ret, mask = cv2.threshold(dst_mask,254,255,cv2.THRESH_BINARY)
    dst=cv2.bitwise_not(cv2.bitwise_xor(rot_thresh,mask))
    dst_gray_brazo_1=cv2.bitwise_not(cv2.bitwise_and(cv2.bitwise_not(dst),cv2.bitwise_not(gray_rot)))
    #brazo2
    filas2,columnas2= brazo2.shape
    mask_blanca=cv2.bitwise_xor(brazo2,cv2.bitwise_not(brazo2))
    M = cv2.getRotationMatrix2D((columnas2/2,filas2/2),ang2,1)
    dst = cv2.warpAffine(brazo2,M,(columnas2,filas2))
    gray_rot=cv2.warpAffine(brazo2_gray,M,(columnas2,filas2))
    dst_mask = cv2.warpAffine(mask_blanca,M,(columnas2,filas2))
    ret, rot_thresh = cv2.threshold(dst,254,255,cv2.THRESH_BINARY)
    ret, mask = cv2.threshold(dst_mask,254,255,cv2.THRESH_BINARY)
    dst=cv2.bitwise_not(cv2.bitwise_xor(rot_thresh,mask))
    dst_gray_brazo_2=cv2.bitwise_not(cv2.bitwise_and(cv2.bitwise_not(dst),cv2.bitwise_not(gray_rot)))

    #proyecciones verticales de los brazos
    #brazo1
    suma_acum=[]
    for ii in range(0,filas):
        suma=0
        for jj in range(0,columnas):
            if dst_gray_brazo_1[ii][jj]!=255:
                suma=suma+dst_gray_brazo_1[ii][jj]
        suma_acum.append(suma)
    for ii in reversed(range(0,len(suma_acum)-1)):
        if suma_acum[ii]!=0:
            index=ii
            break
    dst_gray_brazo_1= dst_gray_brazo_1[0:index, 0:columnas]
    #brazo2
    suma_acum=[]
    for ii in range(0,filas2):
        suma=0
        for jj in range(0,columnas2):
            if dst_gray_brazo_2[ii][jj]!=255:
                suma=suma+dst_gray_brazo_2[ii][jj]
        suma_acum.append(suma)
    for ii in range(0,len(suma_acum)-1):
        if suma_acum[ii]!=0:
            index=ii
            break
    dst_gray_brazo_2= dst_gray_brazo_2[index:filas2, 0:columnas2]
    #determinar cuantas columnas mover para pegar los brazos
    fila1,col1=dst_gray_brazo_1.shape
    count1=0
    for i in range(0,col1):
        if dst_gray_brazo_1[fila1-1][i]!=255:
            count1=count1+1
            index1=i
    fila2,col2=dst_gray_brazo_2.shape
    count2=0
    for i in range(0,col2):
        if dst_gray_brazo_2[0][i]!=255:
            index2=i
            count2=count2+1

    a=int(round(count1/2))
    b=int(round(count2/2))
    index11=index1-a
    index22=index2-b
    offset=abs(int(round((index22-index11))))
    if index11<=index22:#el de arriba(index1) esta mas a la izquierda(termina antes) que index2->la muevo a la derecha


        dst_gray_brazo_1=cv2.copyMakeBorder(dst_gray_brazo_1, top=0, bottom=fila2, left=offset, right=0, borderType= cv2.BORDER_CONSTANT, value=255 )
        dst_gray_brazo_1=dst_gray_brazo_1[0:fila1+fila2,0:col1]
        dst_gray_brazo_2=cv2.copyMakeBorder(dst_gray_brazo_2, top=fila1-2, bottom=2, left=0, right=0, borderType= cv2.BORDER_CONSTANT, value=255 )

    else: #index1>index2 --> el de abajo(index2) esta mas a la izquierda(termina antes) que index1

        dst_gray_brazo_1=cv2.copyMakeBorder(dst_gray_brazo_1, top=0, bottom=fila2, left=0, right=offset, borderType= cv2.BORDER_CONSTANT, value=255 )
        dst_gray_brazo_1=dst_gray_brazo_1[0:fila1+fila2,offset:col1+offset]
        dst_gray_brazo_2=cv2.copyMakeBorder(dst_gray_brazo_2, top=fila1-2, bottom=2, left=0, right=0, borderType= cv2.BORDER_CONSTANT, value=255 )



    nueva=cv2.bitwise_not(cv2.add(cv2.bitwise_not(dst_gray_brazo_1),cv2.bitwise_not(dst_gray_brazo_2)))
    #cv2.namedWindow('aux2', cv2.WINDOW_NORMAL)
    #cv2.imshow("aux2", nueva)
    #cv2.waitKey(0)
    return nueva




def cortarImagen(S,dst,dst_gray,gray,perimetro_normalizado):
    ret, th = cv2.threshold(gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_TRIANGLE)
    #ret, th = cv2.threshold(gray,190,255,cv2.THRESH_BINARY)
    #kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(3,3))
    #th= cv2.morphologyEx(th, cv2.MORPH_CLOSE,kernel)#elimino huecos
    #th= cv2.morphologyEx(th, cv2.MORPH_OPEN, kernel)#elimino ruido del contorno del cromosoma
    th=cv2.bitwise_not(floodfill(cv2.bitwise_not(th))) #rellenar huecos (si quedo alguno)
    th=eliminar_objetos_pequeños(th,20)
    #cv2.namedWindow('aux2', cv2.WINDOW_NORMAL)
    #cv2.imshow("aux2", th)
    ## cv2.waitKey(0)
    img, contours, _ = cv2.findContours(cv2.bitwise_not(th), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    try: #si fitEllipse no obtine 5 puntos de contorno error-> no se porque no encuentra los 5 puntos requeridos, entonces cologo este try
        ellipse = cv2.fitEllipse(contours[0])
        cv2.ellipse(th,ellipse,0,1)
        #print('--')
        #print(len(contours[0]))
        (x,y),(MA,ma),angs = cv2.fitEllipse(contours[0])
        #print(angs)
    except:
        return gray
    #- Si vacio considero el mejor ajuste de la imagen el actual->no la modifico,
    #- perimetro_normalizado:Si en cromosoma es pequeño-> en general no esta curvo -> no nodifico ( para determinar que es pequeño
    #primero determnino el largo del eje medio de todos los cromosomas de la celula de caso de estudio y
    # evaluo solo si el perimetro es mayor a una constante obtenida mediantes test preliminares
    if   S!=-1 and perimetro_normalizado>=0.17:
        offset=S[2]
        index_min=S[4]
        filas,columnas=dst_gray.shape
        brazo1= dst[0:index_min+offset, 0:columnas] # Crop from x, y, w, h -> 100, 200, 300, 400
        # NOTE: its img[y: y + h, x: x + w] and *not* img[x: x + w, y: y + h]
        brazo2= dst[index_min+offset:filas, 0:columnas]
        #determinar como debo girar, si el paso por la linea se da a la derecha o a izquierda de la mitad
        sentido=sentido_giro(dst,index_min,offset,filas,columnas)
        brazo1=cv2.copyMakeBorder(brazo1, top=0, bottom=index_min+offset, left=0, right=0, borderType= cv2.BORDER_CONSTANT, value=255 )
        brazo2=cv2.copyMakeBorder(brazo2, top=index_min+offset, bottom=0, left=0, right=0, borderType= cv2.BORDER_CONSTANT, value=255 )
        p_b1,p_b2=calcular_proyecciones_v(brazo1,brazo2,sentido)
        ang1,ang2=mejor_proyeccion_v(p_b1,p_b2)
        #giro los brasos en la imagen de gris
        brazo1_gray= dst_gray[0:index_min+offset, 0:columnas]
        brazo2_gray= dst_gray[index_min+offset:filas, 0:columnas]
        brazo1_gray=cv2.copyMakeBorder(brazo1_gray, top=0, bottom=index_min+offset, left=0, right=0, borderType= cv2.BORDER_CONSTANT, value=255 )
        brazo2_gray=cv2.copyMakeBorder(brazo2_gray, top=index_min+offset, bottom=0, left=0, right=0, borderType= cv2.BORDER_CONSTANT, value=255 )
        nueva=girar_pegar_brazos(brazo1,brazo2,brazo1_gray,brazo2_gray,ang1,ang2)

        #verificacion
        #antes de retornar la imagen "nueva" verifico que la proyeccion vertical de la nueva sea "menos ancha"
        #que la de la original -> sino retorno la original
        proy_v_nueva=[]
        filas, columnas=nueva.shape
        sum_nueva=0
        for i in range(0, columnas):
           sum_nueva=0
           for j in range(0,filas):
                    if nueva[j][i]!=255:
                        sum_nueva=sum_nueva+1
           proy_v_nueva.append(sum_nueva)
        nueva_no_zero=np.count_nonzero(proy_v_nueva)

        proy_v=[]
        filas, columnas=gray.shape
        for i in range(0, columnas):
           sum=0
           for j in range(0,filas):
                    if gray[j][i]!=255:
                        sum=sum+1
           proy_v.append(sum)
        orig_no_zero=np.count_nonzero(proy_v)
        if nueva_no_zero<orig_no_zero: #retorno la de proyeccion vertical mas angosta
            return nueva
        else:
            return gray
    else:
        return gray

#Endereza la imagen de un cromosoma
#recibe como parametro la imagen de un cromosoma (curvo o recto), y el perimetro-> el algoritmo decide si es requerido
#enderezarlo o no
#retorna la imagen enderezada
def enderezar(img,perimetro_cromos_norm):
    filas,columnas=img.shape
    if filas>columnas:
            bordersize=int(round(filas/2))
    else:
            bordersize=int(round(columnas/2))
    img=cv2.copyMakeBorder(img.copy(), top=bordersize, bottom=bordersize, left=bordersize, right=bordersize, borderType= cv2.BORDER_CONSTANT, value=255 )

    #proyeccion_h2(gray,90)
    proyecciones=calcular_proyecciones(img)
    S,dst,dst_gray=analisis2(proyecciones)
    dst1=cortarImagen(S,dst,dst_gray,img,perimetro_cromos_norm)
    return dst1

#straighten todos los cromosomas de una carpeta
def procesarCarpetaCromos(content_folder,name_example_folder):
    string_path_img=load_all_PATH(content_folder,name_example_folder)
    areas_cromos,perimetros_cromos=calcular_areasCromo_perimsContornoCromo(string_path_img)
    perimetros_cromos_norm=normalizar_vector(perimetros_cromos)
    enderezados=[]
    for j in range(0,len(string_path_img)):#j->clase
        salida=[]
        for i in range(0,len(string_path_img[j])):#i ->img dentro de la clase
            img=load_image(string_path_img[j][i])
            dst1=enderezar(img,perimetros_cromos_norm[j][i])
            indexsF=findStartEndFila(dst1)
            indexsC=findStartEndColumna(dst1)
            salida.append(dst1[indexsF[0]:indexsF[1],indexsC[0]:indexsC[1]])
        enderezados.append(salida)
    return enderezados

#Guardar todos los cromosomas straighten en UNA carpeta
def guardar_enderezados(path_salida_folder,name_example_folder,enderezados):
    for j in range(0,len(enderezados)):
        path=path_salida_folder+"/"+name_example_folder+"/clase"+str(j+1)
        os.makedirs(path, mode=0o777)
        for i in range(0,len(enderezados[j])):
            img=enderezados[j][i]
            #guardo imagen enderezada
            cv2.imwrite(path+"/clase"+str(j+1)+"_"+str(i)+"_"+name_example_folder,img)

#Prosesar Todas las carpetas que se la pasen en la lista
#recibe como parametro la lista de carpetas a procesar
def procesarALLcarpetas(name_example_folder,content_folder,path_salida_folder):
        enderezados=procesarCarpetaCromos(content_folder,name_example_folder)
        guardar_enderezados(path_salida_folder,name_example_folder,enderezados)


def wrapper_enderezar_cromosomas(pathDirectory,path_salida_folder):
    lista_name_example_folder=[name for name in os.listdir(pathDirectory)]

    from joblib import Parallel, delayed
    import multiprocessing
    num_cores = multiprocessing.cpu_count()
    Parallel(n_jobs=num_cores)(delayed(procesarALLcarpetas)(i,pathDirectory,path_salida_folder) for i in lista_name_example_folder)

    #for i in range(0,len(lista_name_example_folder)):
    #        procesarALLcarpetas(lista_name_example_folder[i],pathDirectory,path_salida_folder)

    return "FIN enderezar"
